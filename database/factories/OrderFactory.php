<?php

use App\Models\Order;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Order::class, function (Faker $faker) {
    return [
        'order_number' => $faker->numberBetween(),
        'price' => $faker->numberBetween(),
        'surcharge' => $faker->numberBetween(),
        'sender' => $faker->name,
        'recipient' => $faker->name,
        'manager' => $faker->name,
        'point_issue' => $faker->address,
        'status' => 'Новый',
        'order_owner_id' => 1,
        'order_created_date' => \Carbon\Carbon::now()->format('Y-m-d'),
    ];
});

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 1)->create();
        DB::table('admins')->insert([
            'name' => 'max',
            'email' => 'superadmin@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('password'), // password
            'role' => 'ego',
            'remember_token' => Str::random(10),
        ]);
        DB::table('admins')->insert([
            'name' => 'max',
            'email' => 'kc@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('password'), // password
            'role' => 'kc',
            'remember_token' => Str::random(10),
        ]);
    }
}

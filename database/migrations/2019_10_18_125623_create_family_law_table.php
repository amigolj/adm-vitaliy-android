<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamilyLawTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void 
     */
    public function up()
    {

        Schema::create('family_law', function (Blueprint $table) {
            $table->bigIncrements( 'id');
            $table->string('actual')->nullable();
            $table->string('name')->nullable();
            $table->string('doc')->nullable();
            $table->string('status')->nullable()->default("Новий");
            $table->string('owner')->nullable();
            $table->string('type')->nullable();
            $table->string('rate')->nullable();
            $table->string('tags')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_law');
    }
}

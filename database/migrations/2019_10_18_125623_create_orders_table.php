<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void 
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('order_number')->nullable();
            $table->unsignedInteger('price')->nullable();
            $table->unsignedInteger('surcharge')->nullable();
            $table->text('sender')->nullable();
            $table->text('recipient')->nullable();
            $table->text('manager')->nullable();
            $table->text('point_issue')->nullable();
            $table->dateTime('order_created_date')->nullable();
            $table->dateTime('order_issue_date')->nullable();
            $table->dateTime('order_received_date')->nullable();
            $table->string('status')->nullable();
            $table->string('order_owner_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void 
     */
    public function up()
    {

        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements( 'user_id');
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('gender')->nullable();
            $table->unsignedInteger('birthday')->nullable();
            $table->unsignedInteger('date')->nullable();
            $table->string('city')->nullable();
            $table->string('class')->nullable();
            $table->string('token')->nullable();
            $table->string('password')->nullable();
            $table->string('firebase')->nullable();
            $table->string('online')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}

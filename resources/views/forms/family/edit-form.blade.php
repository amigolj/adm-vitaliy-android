<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="form-edit" class="form-horizontal" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Документ:</label>
                        <div class="col-sm-10">
                            <input id="photo_edit" type="file" accept="image/*" name="photo_edit" />
                            <p class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="price">Назва:*</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" id="name_edit" autofocus required>
                            <p id="nameError" class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Тип документу:*</label>
                        <div class="col-sm-10">
                            <select id="category_edit" class="form-control status_list">
                                <option value="Новий">Новий</option>
                                <option value="З оцінкою">З оцінкою</option>
                            </select>
                            <p id="nameError" class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Тип документу:*</label>
                        <div class="col-sm-10">
                            <select id="category_edit" class="form-control status_list">
                                <option value="Позовна заява">Позовна заява</option>
                                <option value="Заява про видачу судового наказу">Заява про видачу судового наказу</option>
                                <option value="Заява про скасування судового наказу">Заява про скасування судового наказу</option>
                            </select>
                            <p id="nameError" class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Рейтинг:*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="phone_edit" autofocus required>
                            <p id="phoneError" class="errorTitle text-center  alert-danger "></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Теги:*</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control" id="tag_edit" autofocus required>
                        <p id="phoneError" class="errorTitle text-center  alert-danger "></p>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <p  id="errorCommon" class="errorCommon text-center alert alert-danger hidden">Всі поля обовязкові для заповнення</p>
                    <button type="button" class="btn btn-success edit" data-dismiss="modal">
                        <span id="" class='glyphicon glyphicon-check'></span> Редагувати документ
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Закрити
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

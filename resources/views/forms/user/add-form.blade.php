<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="form-add" class="form-horizontal" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="price">Имя:*</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" id="name_add" autofocus required>
                            <p id="nameError" class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">e-mail:*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="email_add" autofocus required>
                            <p id="emailError" class="errorTitle text-center  alert-danger "></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Пароль:*</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password_add" autofocus required>
                            <p id="passwordError" class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Роль:*</label>
                        <div class="col-sm-10">
                            <select id="role_add" class="form-control status_list">
                                <option value="superadmin">Управляющий</option>
                                <option value="ego">ЭГО</option>
                                <option value="kc">КЦ</option>
                            </select>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <p  class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <p  id="errorCommon" class="errorCommon text-center alert alert-danger hidden">Все поля обязательны для заполнения</p>
                    <button type="button" class="btn btn-success add" data-dismiss="modal">
                        <span id="" class='glyphicon glyphicon-check'></span> Додати пользователя
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Закрыть
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

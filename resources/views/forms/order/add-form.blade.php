<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="form-add" class="form-horizontal" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="price">Номер заказа:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="order_number_add" autofocus>
                            <p class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="price">Стоимость:</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="price_add" autofocus>
                            <p class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Доплата:</label>
                        <div class="col-sm-10">
                            <input type="checkbox"  id="checkbox_surcharge" autofocus>
                            <input type="number" class="form-control hidden" id="surcharge_add" autofocus>
                            <p class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Отправитель:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="sender_add" autofocus>
                            <p class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Получатель:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="recipient_add" autofocus>
                            <p class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Пункт выдачи:</label>
                        <div class="col-sm-10">
                            <select id="point_issue_add" class="form-control status_list">
                                @foreach($addresses as $address)
                                    <option value="{{$address}}">{{$address}}</option>
                                @endforeach
                            </select>
{{--                            <select id="point_issue_add" class="form-control status_list">--}}
{{--                                <option value="Домодедовская">Домодедовская</option>--}}
{{--                                <option value="Бауманская">Бауманская</option>--}}
{{--                                <option value="Царицыно">Царицыно</option>--}}
{{--                            </select>--}}
                            <p class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Дата создания:</label>
                        <div class="col-sm-10">
                            <input type="date" min="{{date("Y-m-d")}}" value="{{date('Y-m-d')}}" class="form-control" id="order_created_date_add" autofocus>
                            <p class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
{{--                        <label class="control-label col-sm-2" for="title">статус:</label>--}}
                        <div class="col-sm-10">
                            <select id="status_add" class="form-control status_list">
                                <option selected="selected" value="Новый">Новый</option>
                                <option value="Принят">Принят</option>
                                <option value="Выдан">Выдан</option>
                            </select>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <p class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <p  id="errorCommon" class="errorCommon text-center alert alert-danger hidden">Все поля обязательны для заполнения</p>
                    <button type="button" class="btn btn-success add" data-dismiss="modal">
                        <span id="" class='glyphicon glyphicon-check'></span> Додати заказ
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> закрыть
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

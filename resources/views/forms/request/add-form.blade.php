<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="form-add" class="form-horizontal" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">photo:</label>
                        <div class="col-sm-10">
                            <input id="photo_add" type="file" accept="image/*" name="photo_add" />
                            <p class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="price">Имя:*</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" id="name_add" autofocus required>
                            <p id="nameError" class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">family:*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="family_add" autofocus required>
                            <p id="phoneError" class="errorTitle text-center  alert-danger "></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">phone:*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="phone_add" autofocus required>
                            <p id="phoneError" class="errorTitle text-center  alert-danger "></p>
                        </div>
                    </div>
{{--                    <div class="form-group">--}}
{{--                        <label class="control-label col-sm-2" for="title">category:*</label>--}}
{{--                        <div class="col-sm-10">--}}
{{--                            <select id="category_add" class="form-control status_list">--}}
{{--                                @foreach($categories as $category)--}}
{{--                                    <option value="{{$category->category_id}}">{{$category->name}}</option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                            <p id="nameError" class="errorTitle text-center alert alert-danger hidden"></p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">date:*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="date_add" autofocus required>
                            <p id="genderError" class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">city:*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="city_add" autofocus required>
                            <p id="genderError" class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    
                </form>
                <div class="modal-footer">
                    <p  id="errorCommon" class="errorCommon text-center alert alert-danger hidden">Все поля обязательны для заполнения</p>
                    <button type="button" class="btn btn-success add" data-dismiss="modal">
                        <span id="" class='glyphicon glyphicon-check'></span> Додати мастера
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Закрыть
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

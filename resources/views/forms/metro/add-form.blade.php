<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="form-add" class="form-horizontal" role="form" enctype="multipart/form-data">
                   
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="price">name:*</label>
                        <div class="col-sm-10">
                            <input id="metroName" list="metros">
                            <datalist id="metros">
                                @foreach($metros as $metro)
                                    <option value="{{$metro->name}}" >{{$metro->name}}</option>
                                @endforeach
                            </datalist>
                            <button type="button" id="addMetroStation">Додати станцию</button>
                            <p id="nameError" class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <p  id="errorCommon" class="errorCommon text-center alert alert-danger hidden">Все поля обязательны для заполнения</p>
                    <button type="button" class="btn btn-success add" data-dismiss="modal">
                        <span id="" class='glyphicon glyphicon-check'></span> Додати мастера
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Закрыть
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

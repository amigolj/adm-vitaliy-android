@extends('adminlte::page')
@section('title', 'Система учета')
@section('content')

    <div class="row">  
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
{{--                    <button type="button" class="btn btn-primary add-modal"><i class="far fa-plus-square"></i> Додати адрес</button>--}}
                    <a  href="https://adm.boobot.icu/map" id="addAddressMap" class="btn btn-primary"> Додати адрес</a>
{{--                    <iframe src="/resources/views/map-yandex.html" height="100%"></iframe>--}}
                </div>
                <div class="table-responsive">
                    <table class="table" id="postTable" >
                        <thead>
                        <tr>
                            <th>адрес</th>
                            <th>метро</th>
                        </tr>
                        {{ csrf_field() }}
                        </thead>
                        <tbody id="postTableBodySearchResult" class="hidden">
                        <tr class="item"></tr>
                        </tbody>
                        <tbody  id="postTableBody">
                        
                        @if($addresses)
                            @foreach($addresses as $address)

                                <tr class="item{{$address->place_id}}">
                                    <td id="{{$address->place_id}}" class="td_id ">{{$address->address}}</td>
                                    <td class="list-metros-td">
                                    @if($address->metros)
                                        <ul>
                                    @foreach($address->metros as $metro)
                                        <li>{{$metro['name']}};</li>
                                    @endforeach
                                        </ul>
                                    @endif
                                    </td><td>
                                        <button class="edit-modal btn btn-info"
                                                data-id="{{$address->place_id}}"
                                                data-address="{{$address->address}}"
                                                data-metro="{{$address->metros}}"
                                                >
                                            <span class="glyphicon glyphicon-edit"></span> Редактировать</button>
{{--                                        <button class="delete-modal btn btn-danger"--}}
{{--                                            data-id="{{$address->place_id}}"--}}
{{--                                        >--}}
{{--                                        <span class="glyphicon glyphicon-trash"></span> Удалить--}}
{{--                                        </button>--}}
                                </td>
                            </tr>
                        @endforeach
                        @endif
{{--                        <tr><td colspan="5">{{ $address->links() }}</td></tr>--}}
                        </tbody>
                    </table>
                </div><!-- /.panel-body -->
            </div><!-- /.panel panel-default -->
        </div><!-- /.col-md-8 -->
        @include('forms.address.add-form')
        @include('forms.address.edit-form')
        @include('forms.address.delete-form')
        <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>
        <!-- toastr notifications -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" rel="stylesheet">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

        <link href="{{asset('css/custom.css')}} " rel="stylesheet">
        <script type="text/javascript" src="{{asset('js/address/addAddress.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/address/editAddress.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/address/deleteAddress.js')}}"></script>
{{--        <script type="text/javascript" src="{{asset('js/address/searchAddress.js')}}"></script>--}}
        <script type="text/javascript" src="{{asset('js/validateForm.js')}}"></script>

        <script type="text/javascript">
            window.currentUser = {
                name: "John",
                name2: "John1"
            };
            $.each(window.currentUser, function (index, data) {
               console.log(index);      
            });
            $(document).ready(function() {
                $(document).on('change', '.all_status_list', function() {
                    $('#id').val($(this).data('id'));
                    $('#order_number').val($(this).data('order_number'));
                    $('#manager').val($(this).data('manager'));
                    $('#status').val($(this).val());
                    $('.modal-title').text('Подтверждение');
                    $('#confirmModal').modal('show');
                });

               changeStatusInList();
                
                $('.modal-footer').on('click', '.confirm', function() {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    console.log('ajax'+$("#manager").val());
                    console.log("change manager");
                    $.ajax({
                        type: 'PUT',
                        url: 'orders/' + $("#id").val(),
                        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                        data:{
                            '_token':$('input[name=_token]').val(),
                            'status': $("#status").val(),
                            'manager': $("#manager").val(),
                        },

                        success: function(data) {
                            $('.errorTitle').addClass('hidden');
                            $('.errorContent').addClass('hidden');

                            if ((data.errors)) {
                                setTimeout(function () {
                                    $('#editModal').modal('show');
                                    toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                                }, 500);

                                if (data.errors.title) {
                                    $('.errorTitle').removeClass('hidden');
                                    $('.errorTitle').text(data.errors.title);
                                }
                                if (data.errors.content) {
                                    $('.errorContent').removeClass('hidden');
                                    $('.errorContent').text(data.errors.content);
                                }
                            } else {
                                name_manager = $("#"+data.id).parent().find(".td_name");
                                status_tooltip = $("#"+data.id).parent().find(".td_list");
                                name_manager.text($(".all_status_list").data('manager'));
                                    
                                defineStatus(status_tooltip, data);
                                toastr.success('Статус заказа обновлен!', 'Успешно!', {timeOut: 5000});
                            }
                        }
                    });
                    
                    function defineStatus(status_tooltip, data) {
                        select = $(status_tooltip ).find('select');
                        status_tooltip.attr("title", "");
                        tooltip = status_tooltip.attr("title");
                        if (data.order_created_date){
                            tooltip += "Новый " + data.order_created_date;
                            status_tooltip.attr("title", tooltip);
                        }
                        if (data.order_received_date){
                            tooltip +="\nПринят " + data.order_received_date;
                            status_tooltip.attr("title", tooltip);
                        }
                        if (data.order_issue_date){
                            tooltip += "\nВыдан " + data.order_issue_date;
                            status_tooltip.attr("title", tooltip);
                        }
                     
                    }
                });
            });
            
            function changeStatusInList() {
                $(".all_status_list").each(function() {
                    $(this).val($(this).data('selected'));
                });
            }
        </script>
@stop

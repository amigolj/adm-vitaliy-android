@extends('adminlte::page')
@section('title', 'Система учета')
@section('content')
    
    <div class="row">  
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        @if(\Illuminate\Support\Facades\Auth::user()->role != 'kc')
                            <div class="col-sm-1">
                                <button type="button" class="btn btn-primary add-modal"><i class="far fa-plus-square"></i> Додати заказ</button>
                            </div>
                        @endif
                        <div class="col-sm-1">
                            <a id="btn-download" class="btn btn-success" href="{{ route('download') }}"><i class="far fa-file-excel"></i> Выгрузить</a>
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control" type="search" placeholder="Поиск" id="order-search" name="q"
                                   aria-label="Поиск">
                        </div>
                        <div id="btn_reset_div" class="col-sm-2">
                            <i id="search_reset" class="fas fa-times fa-1x glyphicon"></i>
                            <input id="role" type="hidden" value="{{\Illuminate\Support\Facades\Auth::user()->role}}">
                            <input id="manager" type="hidden" value="{{\Illuminate\Support\Facades\Auth::user()->name}}">
                        </div>
                    </div>
                </div>
                

                <div class="table-responsive">
                    <table class="table" id="postTable" >
                        <thead>
                        <tr>
                            <th>номер заказа</th>
                            <th>стоимость</th>
                            <th>доплата</th>
                            <th>отправитель</th>
                            <th>получатель</th>
                            <th>менеджер</th>
                            <th>пункт выдачи</th>
                            <th>статус</th>
                        </tr>
                        {{ csrf_field() }}
                        </thead>
                        <tbody id="postTableBodySearchResult" class="hidden">
                        <tr class="item"></tr>
                        </tbody>
                        
                        <tbody id="postTableBody">
                        
                        @if($orders)
                        @foreach($orders as $order)
                            @php
                                $status = '';
                                       $status = 'Новый '.$order->order_created_date.PHP_EOL;
                                       if ($order->order_issue_date) {
                                            $status .= 'Выдан '.$order->order_issue_date.PHP_EOL;
                                       }
                                       if ($order->order_received_date) {
                                            $status .= 'Принят '.$order->order_received_date;
                                       }
                            @endphp
                            <tr class="item{{$order->id}}">
                                <td id="{{$order->id}}" class="td_id hidden">{{$order->id}}</td>
                                <td>{{$order->order_number}}</td>
                                <td>{{$order->price}}</td>
                                <td>{{$order->surcharge}}</td>
                                <td>{{$order->sender}}</td>
                                <td>{{$order->recipient}}</td>
                                <td class="td_name">{{$order->manager}}</td>
                                <td>{{$order->point_issue}}</td>
                                <td class="td_list auto-tooltip" 
                                    data-placement="top"
                                    data-toggle="tooltip" 
                                    data-html="true" 
                                    title="{{$status}}">
                                    @if(\Illuminate\Support\Facades\Auth::user()->role == 'superadmin'
                                    || \Illuminate\Support\Facades\Auth::user()->role == 'ego')
                                        {{ $order->status }}
                                    @else
                                        <select  class="all_status_list"
                                                 data-selected="{{$order->status}}"
                                                 data-manager="{{\Illuminate\Support\Facades\Auth::user()->name}}"
                                                 data-id="{{$order->id}}"
                                                 data-order_number="{{$order->order_number}}"
                                        >
                                            <option value="Новый">Новый</option>
                                            <option value="Принят">Принят</option>        
                                            <option value="Выдан">Выдан</option>        
                                        </select>
                                    @endif
                                </td>

                                <td>
                                    @if (\Illuminate\Support\Facades\Auth::user()->role == 'superadmin')
                                    <button class="edit-modal btn btn-info"
                                            data-id="{{$order->id}}"
                                            data-order_number="{{$order->order_number}}"
                                            data-price="{{$order->price}}"
                                            data-surcharge="{{$order->surcharge}}"
                                            data-sender="{{$order->sender}}"
                                            data-recipient="{{$order->recipient}}"
                                            data-manager="{{$order->manager}}"
                                            data-point_issue="{{$order->point_issue}}"
                                            data-order_created_date="{{$order->order_created_date}}"
                                            data-order_issue_date="{{$order->order_issue_date}}"
                                            data-status="{{$order->status}}">
                                        <span class="glyphicon glyphicon-edit"></span> Редактировать</button>
{{--                                        <button class="delete-modal btn btn-danger"--}}
{{--                                            data-id="{{$order->id}}"--}}
{{--                                            data-order_number="{{$order->order_number}}"--}}
{{--                                            data-name="{{$order->manager}}" >--}}
{{--                                        <span class="glyphicon glyphicon-trash"></span> Удалить--}}
{{--                                        </button>--}}
                                </td>
                            </tr>
                            @endif
                        @endforeach
                        @endif
                        <tr><td colspan="5">{{ $orders->links() }}</td></tr>
                        </tbody>
                    </table>
                </div><!-- /.panel-body -->
            </div><!-- /.panel panel-default -->
        </div><!-- /.col-md-8 -->
        @include('forms.order.add-form')
        @include('forms.order.edit-form')
        @include('forms.order.delete-form')
        @include('forms.order.confirm-form')
        <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>
        <!-- toastr notifications -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" rel="stylesheet">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

        <link href="{{asset('css/custom.css')}} " rel="stylesheet">
        <script type="text/javascript" src="{{asset('js/order/addOrder.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/order/editOrder.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/order/deleteOrder.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/order/searchOrder.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/validateForm.js')}}"></script>

        <script type="text/javascript">
            window.currentUser = {
                name: "John",
                name2: "John1"
            };
            $.each(window.currentUser, function (index, data) {
               console.log(index);      
            });
            $(document).ready(function() {
                $(document).on('change', '.all_status_list', function() {
                    $('#id').val($(this).data('id'));
                    $('#order_number').val($(this).data('order_number'));
                    $('#manager').val($(this).data('manager'));
                    $('#status').val($(this).val());
                    $('.modal-title').text('Подтверждение');
                    $('#confirmModal').modal('show');
                });

               changeStatusInList();
                
                $('.modal-footer').on('click', '.confirm', function() {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    console.log('ajax'+$("#manager").val());
                    console.log("change manager");
                    $.ajax({
                        type: 'PUT',
                        url: 'orders/' + $("#id").val(),
                        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                        data:{
                            '_token':$('input[name=_token]').val(),
                            'status': $("#status").val(),
                            'manager': $("#manager").val(),
                        },

                        success: function(data) {
                            $('.errorTitle').addClass('hidden');
                            $('.errorContent').addClass('hidden');

                            if ((data.errors)) {
                                setTimeout(function () {
                                    $('#editModal').modal('show');
                                    toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                                }, 500);

                                if (data.errors.title) {
                                    $('.errorTitle').removeClass('hidden');
                                    $('.errorTitle').text(data.errors.title);
                                }
                                if (data.errors.content) {
                                    $('.errorContent').removeClass('hidden');
                                    $('.errorContent').text(data.errors.content);
                                }
                            } else {
                                name_manager = $("#"+data.id).parent().find(".td_name");
                                status_tooltip = $("#"+data.id).parent().find(".td_list");
                                name_manager.text($(".all_status_list").data('manager'));
                                    
                                defineStatus(status_tooltip, data);
                                toastr.success('Статус заказа обновлен!', 'Успешно!', {timeOut: 5000});
                            }
                        }
                    });
                    
                    function defineStatus(status_tooltip, data) {
                        select = $(status_tooltip ).find('select');
                        status_tooltip.attr("title", "");
                        tooltip = status_tooltip.attr("title");
                        if (data.order_created_date){
                            tooltip += "Новый " + data.order_created_date;
                            status_tooltip.attr("title", tooltip);
                        }
                        if (data.order_received_date){
                            tooltip +="\nПринят " + data.order_received_date;
                            status_tooltip.attr("title", tooltip);
                        }
                        if (data.order_issue_date){
                            tooltip += "\nВыдан " + data.order_issue_date;
                            status_tooltip.attr("title", tooltip);
                        }
                     
                    }
                });
            });
            
            function changeStatusInList() {
                $(".all_status_list").each(function() {
                    $(this).val($(this).data('selected'));
                });
            }
        </script>
@stop

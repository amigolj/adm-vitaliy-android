@extends('adminlte::page')

@section('title', 'ADM')

@section('content')
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-sm-2">
                            <label>Группировка по дате</label>
                            <select id="date_group" class="form-control group_list">
                                <option selected value="0">Все</option>
                                @foreach($dates as $date)
                                    <option value="{{$date}}">{{$date}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <label>Группировка по категории</label>
                            <select id="category_group" class="form-control group_list">
                                <option selected value="0">Все</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->category_id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table" id="postTable" >
                        <thead>
                            <th><span class="col_name">record_id</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">date</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">category</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">start</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">users</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">master</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">status</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">total</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">workplace_id</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">address</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                        </tr>
                        {{ csrf_field() }}
                        </thead>
                        <tbody id="postTableBodySearchResult" class="hidden">
                        <tr class="item"></tr>
                        </tbody>
                        <tbody id="postTableBody">
                        @foreach($requests as $request)
{{--                        places.address--}}
                            <tr class="item{{$request->record_id}}">
                                <td>{{$request->record_id}}</td>
                                <td>{{$request->record_date}}</td>
                                <td>{{$request->workplace->category}}</td>
                                <td>{{$request->start}}</td>
                                <td>{{$request->user->user_id}}</td>
                                <td>{{$request->workshift->user->user_id}}</td>
                                <td>{{$request->status}}</td>
                                <td>{{$request->total}}</td>
                                <td>{{$request->workplace_id}}</td>
                                <td>{{$request->workplace->place->address}}</td>
                                <td>
                                    <button class="edit-modal btn btn-info"
                                            data-record_id="{{$request->record_id}}"
                                            data-start="{{$request->start}}"
                                            data-user="{{$request->user->user_id}}"
                                            data-master="{{$request->workshift->user->user_id}}"
                                            data-status="{{$request->status}}"
                                            data-total="{{$request->total}}"
                                            data-workplace_id="{{$request->workplace_id}}"
                                            data-address="{{$request->workplace->place->address}}"
                                            data-place_id="{{$request->workplace->place->place_id}}"
                                           >
                                    <span class="glyphicon glyphicon-edit"></span> Редактировать
                                    </button>
                                </td>
                            </tr>
                        @endforeach
{{--                        <tr><td colspan="5">{{ $users->links() }}</td></tr>--}}
                        </tbody>
                    </table>
                </div><!-- /.panel-body -->
            </div><!-- /.panel panel-default -->
        </div><!-- /.col-md-8 -->
        <!-- Modal form to add a post -->
{{--        @include('forms.request.add-form')--}}
        <!-- Modmetro form to edit a form -->
        @include('forms.request.edit-form')
        <!-- Modal form to delete a form -->
        @include('forms.request.delete-form')
        <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

        <!-- toastr notifications -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" rel="stylesheet">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        
        <script type="text/javascript" src="{{asset('js/request/addRequest.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/request/editRequest.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/request/deleteRequest.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/request/groupRequest.js')}}"></script>
{{--        <script type="text/javascript" src="{{asset('js/request/sortRequest.js')}}"></script>--}}
{{--        <script type="text/javascript" src="{{asset('js/validateForm.js')}}"></script>--}}
        <!-- AJAX CRUD operations -->
        <script type="text/javascript">
            
        </script>
@stop

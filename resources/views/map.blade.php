@extends('adminlte::page')

@section('title', 'Система учета')

@section('content_header')
    <h1>Панель управления</h1>
@stop

@section('content')

    <form id="form-add" class="form-horizontal" role="form" enctype="multipart/form-data">

        <div class="form-group">
            <label class="control-label col-sm-2" for="price">address:*</label>
            <div class="col-sm-10">
                <input type="text" name="name" class="form-control" id="address_add" autofocus required>
                <p id="nameError" class="errorTitle text-center alert alert-danger hidden"></p>
            </div>
        </div>               
        <div class="form-group">
           
            <div class="col-sm-10">
                <input type="hidden" name="name" class="form-control" id="coords_add" autofocus required>                <p id="nameError" class="errorTitle text-center alert alert-danger hidden"></p>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="title">metro:*</label>
            <div class="col-sm-10">
            </div>
        </div>
    </form>
    <ul id="listStation">
    </ul>
    
    <div class="modal-footer">
     
    </div>
    <iframe id="iframeMap" src="/resources/views/map-yandex.html" ></iframe>
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

    <script type="text/javascript" src="{{asset('js/address/addAddressMap.js')}}"></script>

    <script>
        document.getElementById('iframeMap').onload = function() {
            
        };
        $(document).ready(function(){})
        $("#addAddress").click(function () {
            $("#address_add").val(document.getElementsByTagName('iframe')[0].contentWindow.document.getElementById('addressInput').value);
            $("#coords_add").val(document.getElementsByTagName('iframe')[0].contentWindow.document.getElementById('coords').value);
        })
    </script>
@stop
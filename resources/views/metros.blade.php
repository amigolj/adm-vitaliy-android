@extends('adminlte::page')

@section('title', 'ADM')

@section('content')
    <div class="row">
        <div class="col-md-12 ">
            <p class="header">Кликните по карте, чтобы узнать адрес</p>
            <div id="map"></div>
            <div class="panel panel-default">
                <div class="panel-heading">
                        <button type="button" class="btn btn-primary add-modal"><i class="far fa-plus-square"></i> Додати станцию метро</button>
                </div>
                <div class="table-responsive">
                    <table class="table" id="postTable" >
                        <thead>
                            <th><span class="col_name">name</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                        {{ csrf_field() }}
                        </thead>
                        <tbody id="postTableBodySearchResult" class="hidden">
                        <tr class="item"></tr>
                        </tbody>
                        <tbody id="postTableBody">
                        @foreach($metros as $metro)
                            <tr class="item{{$metro->metro_id}}">
                                <td>{{$metro->name}}</td>
                                <td>
                                    <button class="edit-modal btn btn-info"
                                            data-id="{{$metro->metro_id}}"
                                            data-name="{{$metro->name}}"
                                           >
                                        
                                    <span class="glyphicon glyphicon-edit"></span> Редактировать</button>
{{--                                    @if($metro->role != 'superadmin')--}}
{{--                                    <button class="delete-modal btn btn-danger"--}}
{{--                                            data-metro_id="{{$metro->metro_id}}"--}}
{{--                                            data-name="{{$metro->name}}" >--}}
{{--                                        <span class="glyphicon glyphicon-trash"></span> Удалить--}}
{{--                                    </button>--}}
{{--                                    @endif--}}
                                </td>
                            </tr>
                        @endforeach
{{--                        <tr><td colspan="5">{{ $users->links() }}</td></tr>--}}
                        </tbody>
                    </table>
                </div><!-- /.panel-body -->
            </div><!-- /.panel panel-default -->
        </div><!-- /.col-md-8 -->
        <!-- Modal form to add a post -->
        @include('forms.metro.add-form')
        <!-- Modmetro form to edit a form -->
        @include('forms.metro.edit-form')
        <!-- Modal form to delete a form -->
        @include('forms.metro.delete-form')

      
        
        <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

        <!-- toastr notifications -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" rel="stylesheet">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        
        <script type="text/javascript" src="{{asset('js/metro/addMetro.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/metro/editMetro.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/metro/deleteMetro.js')}}"></script>
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=6e6923c1-b35d-4e42-8104-a2400fbad444" type="text/javascript"></script>
            <style type="text/css">
                                  html, body {
                                      width: 100%;
                                      height: 95%;
                                      margin: 0;
                                      padding: 0;
                                      font-family: Arial;
                                      font-size: 14px;
                                  }
            #map {
                width: 100%;
                height: 95%;
            }
            .header {
                padding: 5px;
            }
        </style>
    {{--        <script type="text/javascript" src="{{asset('js/metro/sortMetro.js')}}"></script>--}}
{{--        <script type="text/javascript" src="{{asset('js/validateForm.js')}}"></script>--}}
        <!-- AJAX CRUD operations -->
        <script type="text/javascript">
            ymaps.ready(init);

            function init() {
                var myPlacemark,
                    myMap = new ymaps.Map('map', {
                        center: [55.753994, 37.622093],
                        zoom: 9
                    }, {
                        searchControlProvider: 'yandex#search'
                    });

                // Слушаем клик на карте.
                myMap.events.add('click', function (e) {
                    var coords = e.get('coords');

                    // Если метка уже создана – просто передвигаем ее.
                    if (myPlacemark) {
                        myPlacemark.geometry.setCoordinates(coords);
                    }
                    // Если нет – создаем.
                    else {
                        myPlacemark = createPlacemark(coords);
                        myMap.geoObjects.add(myPlacemark);
                        // Слушаем событие окончания перетаскивания на метке.
                        myPlacemark.events.add('dragend', function () {
                            getAddress(myPlacemark.geometry.getCoordinates());
                        });
                    }
                    console.log("123");
                    //getAddress(coords);
                });

                // Создание метки.
                function createPlacemark(coords) {
                    return new ymaps.Placemark(coords, {
                        iconCaption: 'поиск...'
                    }, {
                        preset: 'islands#violetDotIconWithCaption',
                        draggable: true
                    });
                }

                // Определяем адрес по координатам (обратное геокодирование).
                function getAddress(coords) {
                    myPlacemark.properties.set('iconCaption', 'поиск...');
                    ymaps.geocode(coords).then(function (res) {
                        var firstGeoObject = res.geoObjects.get(0);

                        myPlacemark.properties
                            .set({
                                // Формируем строку с данными об объекте.
                                iconCaption: [
                                    // Название населенного пункта или вышестоящее административно-территориальное образование.
                                    firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                                    // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
                                    firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                                ].filter(Boolean).join(', '),
                                // В качестве контента балуна задаем строку с адресом объекта.
                                balloonContent: firstGeoObject.getAddressLine()
                            });
                    });
                }
            }

        </script>
            <script type="text/javascript">
            $(document).ready(function() {


       
            // & [kind=<string>]
            // & [rspn=<boolean>]
            // & [ll=<number>, <number>]
            // & [spn=<number>, <number>]
            // & [bbox=<number>,<number>~<number>,<number>]
            // & [format=<string>]
            // & [results=<integer>]
            // & [skip=<integer>]
            // & [lang=<string>]
            // & [callback=<string>]
                
        {{--        $.ajaxSetup({--}}
        {{--            headers: {--}}
        {{--                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
        {{--            }--}}
        {{--        });--}}
        {{--        $.ajax({--}}
        {{--            type: 'GET',--}}
        {{--            url: 'https://geocode-maps.yandex.ru/1.x',--}}
        {{--            headers: {--}}
        {{--                'X-CSRF-TOKEN': "{{ csrf_token() }}",--}}
        {{--                "Content-Type": "text/plain; charset=utf-8"--}}
        {{--            },--}}
        {{--            data: {--}}
        {{--                // '_token':$('input[name=_token]').val(),--}}
        {{--                'geocode': needle,--}}
        {{--                'apikey': needle,--}}
        {{--                'geocode': needle,--}}
        {{--                'geocode': needle,--}}
        {{--                'geocode': needle,--}}
        {{--                'geocode': needle,--}}
        {{--                'geocode': needle,--}}
        {{--                'geocode': needle,--}}
        {{--            },--}}
        
        {{--            success: function (data) {--}}
        {{--                $("#postTableBody").addClass('hidden');--}}
        {{--                $("#postTableBodySearchResult").removeClass('hidden');--}}
        
        
        {{--                if ((data.errors)) {--}}
        {{--                    setTimeout(function () {--}}
        {{--                        $('#editModal').modal('show');--}}
        {{--                        toastr.error('Ошибка поиска', 'Ошибка в данных!', {timeOut: 5000});--}}
        {{--                    }, 500)--}}
        {{--                } else {--}}
        {{--                    alert("ok");--}}
        
        {{--                }--}}
        {{--            }--}}
        {{--        })--}}
            })
            
        </script>
    </div>
@stop

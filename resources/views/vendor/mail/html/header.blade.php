<tr>
    <td class="header">
        <a href="{{ $url }}">
            <img style="vertical-align: middle;" src="https://lh3.googleusercontent.com/-GADsLqzd6sPYdG_zkANvFcNLZsm36rpISYncowzZ6BhMT_0_LzHnqfrsspG7VM0CAhb6jBkmc8KReXyR6ACJfdIvEv6UVTnJLdtcwk4BCKCOyqEz-1X9rBoK8KK0c7HnGiUhVWDPMeNkgw1iwQRejh7t41VCBm-c2srT0qP1xrfbikb3cvsJedeG4ZfrLV5P_EQ_rL4eFOMZdjL1gHpD9A1ChiLx6fVPiEJ1iUssBQ31wZxIIC__P9mgUts-lWY69ZLh4tV_Ct2_7onrNk_P6Ho6cjLeblSmFgwDG3s5Ytp7_6342AfvuLel8UeSE_IIH-yh-KL5J0s8txITEicomlNIE48XwG889n52MXqjkOwqyjnDKT59ws101I7bokQP_6iNqLU5ymTdj55Y3kF0ZAPg91_-odrjeZrGqQtuocepKQvugueuItwlIgLGUZIKBC8fgIWKY9JYQ3Rvzjh46Xr2zAqmR8WXSYDy43rxHB_Fqe2Z-lBdVywf16vumYokA_gEzZmOrxzdJ41N68ORw4DsUU2o4I5j5cgl2PCVgo9j0rn2txIWE-J_GcuEzQ4clBWDUVJY2cORfOGF1vygiVKjY11SvoXYyYU3rl0dh8r6kKBjinWMDnsIF_0g3Q32-0bBdye9CtHwNIfGK0WdN2jlBrcxnagLJ2DMMfYd_PmYwyBbN_-Ygs=s128-no" id="logo" width="48px" alt=""> <span style="vertical-align: middle;"> {{ $slot }} </span>
        </a>
    </td>
</tr>

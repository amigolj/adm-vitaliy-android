@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link href="{{asset('css/custom.css')}} " rel="stylesheet">

    @yield('css')
@stop

@section('body_class', 'login-page')

@section('body')
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Для початку роботи увійдіть</p>
            <form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                {{ csrf_field() }}

                <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                    <input type="text" name="email" id="email" class="form-control" value="{{ old('name') }}"
                           data-mask="" placeholder="пошта">
{{--                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>--}}
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" id="password" name="password" class="form-control"
                           placeholder="{{ 'пароль' }}">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-8">
{{--                           <button type="button" class="form-control " id="sendCode">--}}
{{--                               Отправить код--}} 
{{--                           </button>--}}
                        <span id="fail" >Такой номер не зарегистрирован</span>
                        <span id="success" >Введите полученый код</span>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">
                            {{ "Увійти " }}
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <span id="fail" >Такой номер не зарегистрирован</span>
                        <span id="success" >Введите полученый код</span>
                    </div>
                </div>
            </form>
            <br>
            <p>
{{--                <a href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}" class="text-center">--}}
{{--                    {{ __('adminlte::adminlte.i_forgot_my_password') }}--}}
{{--                </a>--}}
            </p>
            @if (config('adminlte.register_url', 'register'))
                <p>
{{--                    <a href="{{ url(config('adminlte.register_url', 'register')) }}" class="text-center">--}}
{{--                        {{ __('adminlte::adminlte.register_a_new_membership') }}--}}
{{--                    </a>--}}
                </p>
            @endif
        </div>
        <!-- /.login-box-body -->
    </div><!-- /.login-box -->
@stop

@section('adminlte_js')
    @yield('js')
@stop
<script src="{{asset('js/jquery-3.2.1.js')}}"></script>
<script src="{{asset('js/jquery.mask.js')}}"></script>

<script>
    {{--$(document).ready(function($) {--}}
    {{--    $('#name').mask('+0(000)-000-00-00', {placeholder: "+_(___)-___-__-__"});--}}
    {{--    $('#sendCode').on('click', function (e) {--}}
    
    {{--        e.preventDefault();--}}
    
    {{--        var formData = new FormData();--}}
    {{--        formData.append('_token', $('input[name=_token]').val());--}}
    {{--        formData.append('name', $('#name').val());--}}
    {{--        $.ajax({--}}
    {{--            type: 'POST',--}}
    {{--            url: 'users',--}}
    {{--            headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},--}}
    {{--            data: formData,--}}
    {{--            contentType: false,--}}
    {{--            cache: false,--}}
    {{--            processData: false,--}}
    {{--            success: function (data) {--}}
    {{--                $('.errorTitle').addClass('hidden');--}}
    {{--                $('.errorContent').addClass('hidden');--}}
    {{--                $(".errorCommon").addClass('hidden');--}}
    {{--                if ((data.errors)) {--}}
    {{--                    if (data.errors == '2') {--}}
    {{--                        $('#fail').show().fadeOut(2500);--}}
    {{--                    }--}}
    {{--                    if (data.errors.content) {--}}
    {{--                        $('.errorContent').removeClass('hidden');--}}
    {{--                        $('.errorContent').text(data.errors.content);--}}
    {{--                    }--}}
    {{--                } else {--}}
    {{--                    $('#success').show().fadeOut(2500);--}}
    {{--                }--}}
    {{--            },--}}
    {{--        });--}}
    {{--    });--}}
    {{--});--}}

</script>

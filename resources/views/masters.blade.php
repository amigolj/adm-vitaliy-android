@extends('adminlte::page')

@section('title', 'ADM')

@section('content')
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
               
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-sm-3">

                        <button type="button" class="btn btn-primary add-modal"><i class="far fa-plus-square"></i> Додати клиента</button>
                        </div>
                        <div class="col-sm-2">
                            <select id="category_group" class="form-control status_list">
                                <option selected value="0">Все</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->category_id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table" id="postTable" >
                        <thead>
                            <th><span class="col_name">photo</span> <i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">id</span> <i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">name</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">family</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">phone</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">category</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">experience</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">date</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">city</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                        </tr>
                        {{ csrf_field() }}
                        </thead>
                        <tbody id="postTableBodySearchResult" class="hidden">
                        <tr class="item"></tr>
                        </tbody>
                        <tbody id="postTableBody">
                        @foreach($clients as $client)
                            <tr class="item{{$client->user_id}}">
                                <td><img width='100' height='100' src='{{$client->description->photo ?? '-'}}'/></td>
                                <td>{{$client->user_id}}</td>
                                <td>{{$client->name}}</td>
                                <td>{{$client->description->family ?? '-'}}</td>
                                <td>{{$client->phone}}</td>
                                <td>{{$client->description->category ?? '-'}}</td>
                                <td>{{$client->description->experience}}</td>
                                <td>{{$client->user_date}}</td>
                                <td>{{$client->city}}</td>
                                <td>
                                    {{--<button class="show-modal btn btn-success" data-id="{{$client->id}}" data-title="{{$client->title}}" data-content="{{$client->content}}">--}}
                                    {{--<span class="glyphicon glyphicon-eye-open"></span>  class="col_name"Show</button>--}}
                                    <button class="edit-modal btn btn-info"
                                            data-user_id="{{$client->user_id}}"
                                            data-photo="{{$client->description->photo ?? '-'}}"
                                            data-name="{{$client->name}}"
                                            data-family="{{$client->description->family ?? '-'}}"
                                            data-phone="{{$client->phone}}"
                                            data-category="{{$client->description->category ?? '-'}}"
                                            data-date="{{$client->user_date}}"
                                            data-city="{{$client->city}}"
                                           >
                                        
                                    <span class="glyphicon glyphicon-edit"></span> Редактировать</button>
{{--                                    @if($client->role != 'superadmin')--}}
{{--                                    <button class="delete-modal btn btn-danger"--}}
{{--                                            data-user_id="{{$client->user_id}}"--}}
{{--                                            data-name="{{$client->name}}" >--}}
{{--                                        <span class="glyphicon glyphicon-trash"></span> Удалить--}}
{{--                                    </button>--}}
{{--                                    @endif--}}
                                </td>
                            </tr>
                        @endforeach
{{--                        <tr><td colspan="5">{{ $users->links() }}</td></tr>--}}
                        </tbody>
                    </table>
                </div><!-- /.panel-body -->
            </div><!-- /.panel panel-default -->
        </div><!-- /.col-md-8 -->
        <!-- Modal form to add a post -->
        @include('forms.master.add-form')
        <!-- Modal form to edit a form -->
        @include('forms.master.edit-form')
        <!-- Modal form to delete a form -->
        @include('forms.master.delete-form')

        
        <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

        <!-- toastr notifications -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" rel="stylesheet">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        
        <script type="text/javascript" src="{{asset('js/master/addMaster.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/master/editMaster.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/master/deleteMaster.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/master/sortMaster.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/master/groupMaster.js')}}"></script>
{{--        <script type="text/javascript" src="{{asset('js/validateForm.js')}}"></script>--}}
        <!-- AJAX CRUD operations -->

     
    </div>

@stop

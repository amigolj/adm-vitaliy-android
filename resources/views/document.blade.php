@extends('adminlte::page')

@section('title', 'БПД')

@section('content_header')
    <h1>Документ {{$document->name}}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-3">
    <table class="table table-doc">
        <tbody>
        <tr>
            <td>Дата створенная</td>
            <td>06.12.2019 </td>
        </tr>
        <tr class="">
            <td>Дата зміни</td>
            <td>08.12.2019 </td>
        </tr>
        <tr class="">
            <td>Судове рішення</td>
            <td> <a target="_blank" href="http://reyestr.court.gov.ua/Review/71247069">Справа № 344/10741/17</a> </td>
        </tr>
        </tbody>
        </table>
        </div>
    </div>
    <table class="table">
        <thead>
        <th> <h3>Законодавство</h3></th>
        </thead>
        <thead>
            <th>Актуальність</th>
            <th>Закон</th>
            <th>Посилання</th>
        </thead>
        <tbody>
        <tr>
            <td>актуальна</td>
            <td>п.3 ст.41 СК України </td>
            <td><a href="https://zakon.rada.gov.ua/laws/show/2947-14#n220"> https://zakon.rada.gov.ua/laws/show/2947-14#n220</a></td>
        </tr>
        <tr class="danger">
            <td>зміна</td>
            <td>ст.3 ЦПК України </td>
            <td><a href="https://zakon.rada.gov.ua/laws/show/1618-15">https://zakon.rada.gov.ua/laws/show/1618-15</a></td>
        </tr>
        <tr class="danger">
            <td>зміна</td>
            <td>ст. 118 ЦПК України  </td>
            <td><a href="https://zakon.rada.gov.ua/laws/show/1618-15">https://zakon.rada.gov.ua/laws/show/1618-15</a></td>
        </tr>
     
        </tbody>
    </table>
    <ul id="listStation">
        <button class="edit-modal btn btn-info"
                data-id="{{$document->id}}"
        >

            <span class="glyphicon glyphicon-edit"></span><a class="download_link" href="https://legaltech.boobot.icu/document"> Редагувати</a></button>
        <button class="btn btn-success"
                data-id="{{$document->id}}">
            <span class="glyphicon glyphicon-download"><a class="download_link" href="{{$document->doc}}"> Скачати</a></span>
        </button>
    </ul>
    <div class="modal-footer">
    </div>

    <script>
        document.getElementById('iframeMap').onload = function() {

        };
        $(document).ready(function(){});
        $("#addAddress").click(function () {
            })
    </script>
@stop
@extends('adminlte::page')

@section('title', 'ADM')

@section('content')
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                        <button type="button" class="btn btn-primary add-modal"><i class="far fa-plus-square"></i> Додати сотрудника</button>
                </div>
                <div class="table-responsive">
                    <table class="table" id="postTable" >
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Имя</th>
                            <th>email</th>
                            <th>роль</th>
                        </tr>
                        {{ csrf_field() }}
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            @php
                                $role = "Управляющий";
                            if ($user->role == 'kc'){
                                $role = 'KЦ';
                            } 
                            if ($user->role == 'ego'){
                                $role = 'ЭГО';
                            }
                            @endphp
                            <tr class="item{{$user->id}}">
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$role}}</td>
                                <td>
                                    {{--<button class="show-modal btn btn-success" data-id="{{$user->id}}" data-title="{{$user->title}}" data-content="{{$user->content}}">--}}
                                    {{--<span class="glyphicon glyphicon-eye-open"></span> Show</button>--}}
                                    <button class="edit-modal btn btn-info"
                                            data-id="{{$user->id}}"
                                            data-name="{{$user->name}}"
                                            data-email="{{$user->email}}"
                                            data-role="{{$user->role}}"
                                           >
                                        
                                    <span class="glyphicon glyphicon-edit"></span> Редактировать</button>
{{--                                    @if($user->role != 'superadmin')--}}
{{--                                    <button class="delete-modal btn btn-danger"--}}
{{--                                            data-id="{{$user->id}}"--}}
{{--                                            data-name="{{$user->name}}" >--}}
{{--                                        <span class="glyphicon glyphicon-trash"></span> Удалить--}}
{{--                                    </button>--}}
{{--                                    @endif--}}
                                </td>
                            </tr>
                        @endforeach
                        <tr><td colspan="5">{{ $users->links() }}</td></tr>
                        </tbody>
                    </table>
                </div><!-- /.panel-body -->
            </div><!-- /.panel panel-default -->
        </div><!-- /.col-md-8 -->
        <!-- Modal form to add a post -->
        @include('forms.user.add-form')
        <!-- Modal form to edit a form -->
        @include('forms.user.edit-form')
        <!-- Modal form to delete a form -->
        @include('forms.user.delete-form')
        <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aADMmvFpqI=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

        <!-- toastr notifications -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" rel="stylesheet">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        
        <script type="text/javascript" src="{{asset('js/user/addUser.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/user/editUser.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/user/deleteUser.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/validateForm.js')}}"></script>
        <!-- AJAX CRUD operations -->
        <script type="text/javascript">
            
        </script>
@stop

@extends('adminlte::page')

@section('title', 'ADM')

@section('content')
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-primary add-modal"><i class="far fa-plus-square"></i> Додати рабочее место</button>
                        </div>
                        <div class="col-sm-2">
                            <select id="workplace_group" class="form-control status_list">
                                <option selected value="0">Все</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->category_id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    @foreach($places as $place)
                        @if(count($place->workplaces)>0)
                        <table class="table" id="postTable" >
                        <thead>
                            <tr class="head_address"><td  colspan="4"><h3>{{$place->address}}</h3></td></tr>
                            <th><span class="col_name">workplace_id</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">category</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                      
                        {{ csrf_field() }}
                        </thead>
                        <tbody id="postTableBodySearchResult" class="hidden">
                        <tr class="item"></tr>
                        </tbody>
                        <tbody id="postTableBody{{$place->place_id}}" class="allWorkPlacesTables">
                            @foreach($place->workplaces as $workplace)
                                <tr class="item{{$place->place_id.$workplace->workplace_id}}">
                                    <td>{{$workplace->workplace_id}}</td>
                                    <td>{{$workplace->category}}</td>
                                    <td>
                                    <button class="edit-modal btn btn-info"
                                            data-place_id="{{$place->place_id}}"
                                            data-category="{{$workplace->category}}"
                                            data-workplace_id="{{$workplace->workplace_id}}"
                                           >
                                        
                                    <span class="glyphicon glyphicon-edit"></span> Редактировать</button>
{{--                                    @if($place->role != 'superadmin')--}}
{{--                                    <button class="delete-modal btn btn-danger"--}}
{{--                                            data-place_id="{{$place->place_id}}"--}}
{{--                                            data-workplace_id="{{$workplace->workplace_id}}"--}}
{{--                                            data-name="{{$place->address}}" >--}}
{{--                                        <span class="glyphicon glyphicon-trash"></span> Удалить--}}
{{--                                    </button>--}}
{{--                                    @endif--}}
                                    </td>
                                </tr>
                            @endforeach
                    
{{--                        <tr><td colspan="5">{{ $users->links() }}</td></tr>--}}
                        </tbody>
                    </table>
                        @endif
                    @endforeach
                </div><!-- /.panel-body -->
            </div><!-- /.panel panel-default -->
        </div><!-- /.col-md-8 -->
        <!-- Modal form to add a post -->
        @include('forms.workplace.add-form')
        <!-- Modmetro form to edit a form -->
        @include('forms.workplace.edit-form')
        <!-- Modal form to delete a form -->
        @include('forms.workplace.delete-form')
        <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

        <!-- toastr notifications -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" rel="stylesheet">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        
        <script type="text/javascript" src="{{asset('js/workplace/addWorkplace.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/workplace/editWorkplace.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/workplace/deleteWorkplace.js')}}"></script>
{{--        <script type="text/javascript" src="{{asset('js/workplace/sortWorkplace.js')}}"></script>--}}
        <script type="text/javascript" src="{{asset('js/workplace/groupWorkplace.js')}}"></script>
{{--        <script type="text/javascript" src="{{asset('js/validateForm.js')}}"></script>--}}
        <!-- AJAX CRUD operations -->
        <script type="text/javascript">
            
        </script>
@stop

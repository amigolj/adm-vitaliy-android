@extends('adminlte::page')

@section('title', 'БПД')

@section('content')
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                        <button type="button" class="btn btn-primary add-modal"><i class="far fa-plus-square"></i> Додати документ</button>
                </div>
                <div class="table-responsive">
                    <table class="table" id="postTable" >
                        <thead>
                        <tr>
{{--                            <th><span class="col_name">id</span> <i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>--}}
                            <th><span class="col_name">актуальність</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">автор</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">назва</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">статус</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">тип</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">рейтинг</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                            <th><span class="col_name">теги</span><i class="fas fa-sort-up hidden"></i><i class="fas fa-sort-down hidden"></i></th>
                        </tr>
                        {{ csrf_field() }}
                        </thead>
                        <tbody id="postTableBodySearchResult" class="hidden">
                        <tr class="item"></tr>
                        </tbody>
                        <tbody id="postTableBody">
                        @foreach($families as $family)
                            <tr class="item{{$family->id}}">
                                <td>{{ $family->actual}}</td>
                                <td>{{ $family->owner}}</td>
                                <td>{{ $family->name}}</td>
                                <td>{{ $family->status}}</td>
                                <td>{{ $family->type}}</td>
                                <td>{{ $family->rate}}</td>
                                <td>{{ $family->tags}}</td>
                                <td>
                                    <button class="edit-modal btn btn-info"
                                            data-id="{{$family->id}}"
                                           >
                                        
                                        <span class="glyphicon glyphicon-edit"></span><a class="show_link" href="{{route('document.show', $family->id)}}"> Посмотреть</a></button>
                                    <button class="btn btn-success"
                                            data-id="{{$family->id}}">
                                        <span class="glyphicon glyphicon-download"><a class="download_link" href="{{$family->doc}}"> Скачати</a></span> 
                                    </button>
                                    <button class="delete-modal btn btn-danger"
                                            data-id="{{$family->id}}">
                                        <span class="glyphicon glyphicon-trash">Видалити</span>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.panel-body -->
            </div><!-- /.panel panel-default -->
        </div><!-- /.col-md-8 -->
        <!-- Modal form to add a post -->
        @include('forms.family.add-form')
        <!-- Modal form to edit a form -->
        @include('forms.family.edit-form')
        <!-- Modal form to delete a form -->
        @include('forms.family.delete-form')
        <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

        <!-- toastr notifications -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" rel="stylesheet">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        
        <script type="text/javascript" src="{{asset('js/family/addMaster.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/family/editMaster.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/family/deleteMaster.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/family/sortMaster.js')}}"></script>
{{--        <script type="text/javascript" src="{{asset('js/validateForm.js')}}"></script>--}}
        <!-- AJAX CRUD operations -->
        <script type="text/javascript">
            
        </script>
@stop

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\FamilyLaw;
use App\Models\Record;
use App\Notifications\OrderNotification;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

Route::get('/', function () {

    return redirect('login'); 
});

Route::get('/test', function () {
    dd(\Illuminate\Support\Facades\Route::current()->uri());
    return redirect('login');
});

Auth::routes(['register' => false]);
Route::get('logout', function (){ 
    return redirect('login');
});
Route::resource('users', 'UserController');

Route::group(['middleware' => 'auth'], function () {

    Route::resource('families-law', 'FamilyLawController');
    Route::get('document/{id}', 'FamilyLawController@documentShow')->name('document.show');
    Route::get('/home', 'HomeController@index')->name('home');
});
Route::fallback(function () {
    return 'ok';
});
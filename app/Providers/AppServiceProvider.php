<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use Illuminate\Contracts\Events\Dispatcher;
//
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            
            $event->menu->add('Галузі права');

//            if (Auth::user()->role == 'superadmin') {

                $event->menu->add([
                    'text' => 'Сімейне право',
                    'url' => 'families-law',
                    'icon' => 'fas fa-user-friends',
                ]);
             
                
//                $event->menu->add('Администрирование');
//                $event->menu->add([
//                    'text' => 'Пользователи',
//                    'url' => 'users',
//                    'icon' => 'fas fa-user-friends',
//                ]);
               
//            } else {
//                $event->menu->add([
//                    'text' => 'Заказы',
//                    'url' => 'orders',
//                    'icon' => 'fas fa-shipping-fast',
//                ]);
//            }
        });
         
        Schema::defaultStringLength(191);
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RecordResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'record_id' => $this->record_id,
            'record_date' => $this->record_date,
            'category' => $this->workplace->category,
            'start' => $this->start,
            'user' => $this->user->user_id,
            'master' => $this->workshift->user->user_id,
            'status' => $this->status,
            'total' => $this->total,
            'workplace_id' => $this->workplace_id,
            'address' => $this->workplace->place->address,
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $forUserRole = 'ЭГО';
        if ($this->role == 'superadmin') 
        {
            $forUserRole = 'Управляющий';    
        }
        if ($this->role == 'kc')
        {
            $forUserRole = 'KЦ';
        }
        return [
            'user_id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password,
            'role' => $this->role,
            'forUserRole' => $forUserRole,
        ];
    }
}

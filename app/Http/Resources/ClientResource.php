<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'doc' => $this->doc,
            'actual' => 'Актуальний',
            'owner' => Auth::user()->name,
            'name' => $this->name,
            'status' => 'Новий',
            'type' => $this->type,
            'rate' => 4,
            'tags' => $this->tags,
        ];
    }
}

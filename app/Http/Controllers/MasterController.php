<?php

namespace App\Http\Controllers;

use App\Helpers\DocumentHelper;
use App\Http\Resources\ClientResource;
use App\Models\Category;
use App\Models\FamilyLaw;
use App\Models\Description;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class MasterController extends Controller
{

    use DocumentHelper;

    protected $rules =
        [
            'name' => 'required|min:2|max:32',
//            'logo' => 'dimensions:min_width=100,min_height=100'
        ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = FamilyLaw::where('class','masters')->with('description')->get();
        $categories = Category::all();
       
        return view('masters',['clients' => $clients, 'categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filename = $this->saveImage($request);
        Log::alert($filename);
        $input = $request->toArray();
        $input['photo'] = $filename;
        $input['class'] = 'masters';
        $input['experience'] = strtotime($request->get('experience'));
        $validator = Validator::make($input, $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }

        try {
            $client = FamilyLaw::create($input);
            $description = new Description($input);
            $client->description()->save($description);
        } catch (\Throwable $e) {
            return response()->json(
                ['errors' => $e->getMessage()]);
        }
        return response()->json(new ClientResource($client));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $input = $request->all();
            $input['experience'] = strtotime($request->get('experience'));

            $desk = Description::updateOrCreate(['user_id' =>$id],
                $input);
            $client = FamilyLaw::updateOrCreate(['user_id' =>$id],$request->all());
            return response(new ClientResource($client));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = FamilyLaw::findOrFail($id);
        $post->delete();

        return response()->json($post);
    }

    public function sort(Request $request)
    {
        if ($request->get('sortType') == 1) {
            $clients = DB::table('users')
                ->join('descriptions', 'descriptions.user_id', '=', 'users.user_id')->where('class','=','masters')
                ->select(DB::raw("users.*, descriptions.*, DATE_FORMAT(FROM_UNIXTIME(`user_date`), '%m-%d-%Y') as user_date"))
                ->get();
        } else {
            $clients = DB::table('users')
                ->join('descriptions', 'descriptions.user_id', '=', 'users.user_id')->where('class','=','masters')
                ->select(DB::raw("users.*, descriptions.*, DATE_FORMAT(FROM_UNIXTIME(`user_date`), '%m-%d-%Y') as user_date"))
                ->where('class','=','masters')->orderBy($request->get('fieldName'), $request->get('sortType'))->get();
        }
        return response($clients);
    }
    
    public function sortByDate($request)
    {
//        $clients = DB::table('users')
//            ->join('descriptions', 'descriptions.user_id', '=', 'users.user_id')->where('class','=','masters')
//            ->where('class','=','masters')
//            ->orderBy($request->get('fieldName'), $request->get('sortType'))->get();
//
//        ->whereRaw("DATE_FORMAT(FROM_UNIXTIME(`record_date`), '%Y-%m-%d') = ?", [date('Y-m-d',strtotime($request->get('fieldName')))])
//        ->get();
    }


    public function group(Request $request)
    {
        if ($request->get('fieldName') == 0) {
            $masters = DB::table('users')
                ->where('class', '=', 'masters')->get();
        } else {
            $masters = DB::table('users')
                ->join('descriptions', 'descriptions.user_id', '=', 'users.user_id')
                ->where('class', '=', 'masters')
                ->where('category', '=', $request->get('fieldName'))->get();
        }
        
        return response($masters);
    }
}

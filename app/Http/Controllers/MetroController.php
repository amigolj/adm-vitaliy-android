<?php

namespace App\Http\Controllers;

use App\Http\Resources\ClientResource;
use App\Models\Metro;
use App\Models\Description;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class MetroController extends Controller
{


    protected $rules =
        [
            'name' => 'required|min:2|max:32',
//            'logo' => 'dimensions:min_width=100,min_height=100'
        ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $metros = Metro::all();
        
        return view('metros',['metros' => $metros]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->toArray();
        $validator = Validator::make($input, $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }

        try {
            $metro = Metro::create($input);
        } catch (\Throwable $e) {
            return response()->json(
                ['errors' => $e->getMessage()]);
        }
        return response()->json($metro);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {

            return response(Metro::updateOrCreate(['metro_id' =>$id],$request->all()));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Metro::findOrFail($id);
        $post->delete();

        return response()->json($post);
    }

    public function sort(Request $request)
    {
        if ($request->get('sortType') == 1) {
            $clients = DB::table('metros')->get();
        } else {
            $clients = DB::table('metros')->orderBy($request->get('fieldName'), $request->get('sortType'))->get();
        }
        //        orderBy('name', 'desc')->
        return response($clients);
    }

}

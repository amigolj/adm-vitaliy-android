<?php

namespace App\Http\Controllers;

use App\Helpers\LoginHelper;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    use LoginHelper;
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try {
            if(!$this->updateUser($request->all())){
                return response()->json(array('errors' => 2));
            };
        } catch (\Throwable $e) {
            Log::error($e);
            return response()->json(array('errors' => $e->getMessage()));
        }
        return response()->json(1);
    }
    
}

<?php

namespace App\Http\Controllers;

use App\Helpers\ExcelHelper;
use App\Helpers\OrderHelper;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\Helpers\AddressHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Order;
use Illuminate\Support\Facades\DB;

class OrderSearchController extends Controller
{
    
    use OrderHelper;
    
    public function orderSearch(Request $request)
    {
        
        $needle = $request->get('needle');
        $results =  DB::table('orders')
            ->select('*')
            ->where('order_number', 'like', $needle."%")
            ->orWhere('price', 'like',  $needle. '%')
            ->orWhere('surcharge','like',  $needle. '%')
            ->orWhere('sender', 'like',  $needle. '%')
            ->orWhere('recipient', 'like',  $needle. '%')
            ->orWhere('manager', 'like',  $needle. '%')
            ->orWhere('point_issue', 'like',  $needle. '%')
//          ->orWhere('order_created_date', 'like',  $needle. '%')
//          ->orWhere('order_issue_date', 'like',  $needle. '%')
//          ->orWhere('order_received_date', 'like',  $needle. '%')
            ->orWhere('order_owner_id', 'like',  $needle. '%')
            ->get()->toArray();
 
        foreach ($results as &$result) {
            $result = $this->addRoleToOrder($result);
        }
        return response($results);
    }
}

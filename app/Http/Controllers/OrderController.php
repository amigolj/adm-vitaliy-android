<?php

namespace App\Http\Controllers;

use App\Helpers\ExcelHelper;
use App\Helpers\OrderHelper;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use App\Helpers\AddressHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Order;
use App\Notifications\OrderNotification;


class OrderController extends Controller
{
    
    use OrderHelper;

    protected $rules = [];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::orderBy('id', 'desc')->paginate(10);
        $attributes = array_keys($orders[0]->getAttributes());
        
        return view('orders',['orders' => $orders, 'addresses'=>AddressHelper::$addresses, 'attributes' =>$attributes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        $data = $request->all();
        $data['status'] = 'Новый';
        $data['order_owner_id'] = Auth::id();
        $order = Order::create($data);
        $order['role'] = Auth::user()->role;
        return response()->json( $order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $order = Order::updateOrCreate(['id' =>$id], $this->handleDate($request));
            $order['role'] = Auth::user()->role;
            $user = User::find($order['order_owner_id']);
            Notification::send($user, new OrderNotification($order['order_number'], $order['status'], $user->name));
            return response()->json($order);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Order::findOrFail($id);
        $post->delete();

        return response()->json($post);
    }
    
    public function downloadExcel()
    {
        try {
            (new ExcelHelper())->createFile();
            $file = storage_path().'/export.xlsx';
            $headers = array(
                'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            );
            return \response()->download($file, 'export-'.date('d-m-Y').'.xlsx', $headers);
        } catch (\Throwable $exception) {
            Log::alert(
                $exception->getMessage()
                .$exception->getFile()
                .$exception->getLine());
            return abort(500);
        }
        return 'ok';
    }
}

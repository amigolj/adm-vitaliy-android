<?php

namespace App\Http\Controllers;

use App\Helpers\DocumentHelper;
use App\Http\Resources\RecordResource;
use App\Models\Category;
use App\Models\FamilyLaw;
use App\Models\Place;
use App\Models\Record;
use App\Models\Workplace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class RequestController extends Controller
{

    use DocumentHelper;

    protected $rules =
        [
//            'name' => 'required|min:2|max:32',
//            'logo' => 'dimensions:min_width=100,min_height=100'
        ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requests= Record::with('workplace.place','workshift.user', 'user')->get();
        $categories = Category::all();
        $clients = FamilyLaw::all();
        $places = Place::all();
        $workplaces = Workplace::all();
        $dates = Record::distinct('record_date')->get(); //refactor
        $arr = [];
        foreach ($dates as $date) {
            $arr[$date->record_date] =  $date->record_date;
        }
        
        return view('requests',[
            'requests' => $requests,
            'categories' => $categories,
            'dates' => $arr,
            'places' => $places,
            'workplaces' => $workplaces,
            'clients' => $clients
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->toArray();
        $validator = Validator::make($input, $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }

        try {
            $request = Record::create($input);
//            $description = new Description($input);
//            $client->description()->save($description);
        } catch (\Throwable $e) {
            return response()->json(
                ['errors' => $e->getMessage()]);
        }
        return response()->json(new RecordResource($request));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $result = Record::updateOrCreate(['record_id' =>$id],$request->all());
//            Workplace::updateOrCreate(['record_id' =>$request->get('workplace_id')],$request->all());
//            print_r($result->user->toArray());
//            $result2->user()->update(['user_id' =>$request->get('user_id')]);
//            $client = new Client();
//            $client->update(
//                ['user_id' => $request->get('user_id')],
//                ['user_id' => $request->get('user_id')]
//                );
            return response(new RecordResource($result));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function destroy($id)
//    {
//        $post = Client::findOrFail($id);
//        $post->delete();
//
//        return response()->json($post);
//    }

    public function sort(Request $request)
    {
        if ($request->get('sortType') == 1) {
            $requests= DB::table('clients')->get();
        } else {
            $requests= DB::table('clients')->orderBy($request->get('fieldName'), $request->get('sortType'))->get();
        }
        //        orderBy('name', 'desc')->
        return response($requests);
    }


    public function groupByCategory(Request $request)
    {
        if ($request->get('fieldName') == 0) {
            $workplaces = DB::table('records')
                ->join('workplaces', 'records.workplace_id', '=', 'workplaces.workplace_id')
                ->where('category', '=', '1')->get();
        } else {
            $workplaces = DB::table('records')
                ->join('workplaces', 'records.workplace_id', '=', 'workplaces.workplace_id')
                ->join('places', 'places.place_id', '=', 'workplaces.place_id')
                ->join('workshifts', 'workshifts.workshift_id', '=', 'records.workshift_id')
                ->join('users', 'users.user_id', '=', 'workshifts.user_id')
                ->select(
                    'records.record_id',
                    'records.record_date',
                    'workplaces.category',
                    'records.start',
                    'records.user_id as user',
                    'workshifts.user_id as master',
                    'records.status',
                    'records.total',
                    'records.workplace_id',
                    'places.address'
                    
                )
                ->where('workplaces.category', '=', $request->get('fieldName'))->get();
        }
        $arr =[];
        foreach ($workplaces as &$workplace){
            $workplace->record_date = $workplace->record_date;
        }
        return response($workplaces);
    }

    public function groupByDate(Request $request)
    {
        if ($request->get('fieldName') == 0) {
            $workplaces = DB::table('records')
                ->join('workplaces', 'records.workplace_id', '=', 'workplaces.workplace_id')
                ->join('places', 'places.place_id', '=', 'workplaces.place_id')
                ->join('workshifts', 'workshifts.workshift_id', '=', 'records.workshift_id')
                ->join('users', 'users.user_id', '=', 'workshifts.user_id')
                ->select(
                    'records.record_id',
                    'records.record_date',
                    'workplaces.category',
                    'records.user_id as user',
                    'workshifts.user_id as master',
                    'records.status',
                    'records.total',
                    'records.workplace_id',
                    'places.address',
                    DB::raw("DATE_FORMAT(FROM_UNIXTIME(records.start), '%Y-%m-%d %H:%i:%s') as start")
                )
                ->where('category', '=', '1')->get();
        } else {
            $workplaces = DB::table('records')
                ->join('workplaces', 'records.workplace_id', '=', 'workplaces.workplace_id')
                ->join('places', 'places.place_id', '=', 'workplaces.place_id')
                ->join('workshifts', 'workshifts.workshift_id', '=', 'records.workshift_id')
                ->join('users', 'users.user_id', '=', 'workshifts.user_id')
                ->select(
                    'records.record_id',
                    'records.record_date',
                    'workplaces.category',
                    'records.user_id as user',
                    'workshifts.user_id as master',
                    'records.status',
                    'records.total',
                    'records.workplace_id',
                    'places.address',
                    DB::raw("DATE_FORMAT(FROM_UNIXTIME(records.start), '%Y-%m-%d %H:%i:%s') as start")
                )
                ->whereRaw("DATE_FORMAT(FROM_UNIXTIME(`record_date`), '%Y-%m-%d') = ?", [date('Y-m-d',strtotime($request->get('fieldName')))])
                ->get();
        }
        $arr =[];
        foreach ($workplaces as &$workplace){
            $workplace->record_date = $workplace->record_date;
        }
        return response($workplaces);
    }
}

<?php

namespace App\Http\Controllers;

use App\Helpers\DocumentHelper;
use App\Http\Resources\ClientResource;
use App\Models\FamilyLaw;
use App\Models\Description;
use Carbon\Carbon;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class FamilyLawController extends Controller
{
    use DocumentHelper;


    protected $rules =
        [
//            'name' => 'required|min:2|max:32',
//            'logo' => 'dimensions:min_width=100,min_height=100'
        ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $families = FamilyLaw::all();
     
        return view('family',['families' => $families]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $filename = $this->saveImage($request);
        Log::alert($filename);
        $input = $request->toArray();
        $input['actual'] = "Актуальний";
        $input['doc'] = $filename;
        $input['owner'] = Auth::user()->name;
        $input['status'] = 'Новий';
        $input['rate'] = 4;
        Log::alert("123");
        $validator = Validator::make($input, $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }

        try {
            $client = FamilyLaw::create($input);
        } catch (\Throwable $e) {
            return response()->json(
                ['errors' => $e->getMessage()]);
        }
        return response()->json(new ClientResource($client));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
                   
            return response(FamilyLaw::updateOrCreate(['user_id' =>$id],$request->all()));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $id)
    {
        $post = FamilyLaw::findOrFail($id);
        $post->delete();

        return response()->json($post);
    }

    public function sort(Request $request)
    {
        if ($request->get('sortType') == 1) {
            $clients = DB::table('users')
                ->where('class','users')
                ->select(DB::raw("users.*, DATE_FORMAT(FROM_UNIXTIME(`user_date`), '%m-%d-%Y') as user_date"))
                ->get();
        } else {
            $clients = DB::table('users')
                ->select(DB::raw("users.*, DATE_FORMAT(FROM_UNIXTIME(`user_date`), '%m-%d-%Y') as user_date"))
                ->where('class','users')->orderBy($request->get('fieldName'), $request->get('sortType'))->get();
        }
        return response($clients);
    }


    public function documentShow($id)
    {
        $document = FamilyLaw::findOrFail($id);
     
        return view('document',[
            'document' => $document
        ]);
    }
}

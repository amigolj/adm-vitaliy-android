<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Place;
use App\Models\Workplace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class WorkplaceController extends Controller
{


    protected $rules =
        [
//            'name' => 'required|min:2|max:32',
//            'logo' => 'dimensions:min_width=100,min_height=100'
        ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $places = Place::all();
        $categories = Category::all();
        
        return view('workplaces',['places' => $places, 'categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->toArray();
        $validator = Validator::make($input, $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }

        try {
            $workplace = Workplace::create($input);
        } catch (\Throwable $e) {
            return response()->json(
                ['errors' => $e->getMessage()]);
        }
        $workplace->load('place')->place();
        return response()->json($workplace);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {

            return response(Workplace::updateOrCreate(['workplace_id' => $id], $request->all()));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Workplace::findOrFail($id);
        $post->delete();

        return response()->json($post);
    }

    public function sort(Request $request)
    {
        if ($request->get('sortType') == 1) {
            $clients = DB::table('workplaces')->get();
        } else {
            $clients = DB::table('workplaces')->orderBy($request->get('fieldName'), $request->get('sortType'))->get();
        }
        return response($clients);
    }

    public function group(Request $request)
    {
        if ($request->get('fieldName') == 0) {
            $workplaces = DB::table('workplaces')
                ->join('places', 'places.place_id', '=', 'workplaces.place_id')
                ->get();
        } else {
            $workplaces = DB::table('places')
                ->join('workplaces', 'places.place_id', '=', 'workplaces.place_id')
                ->where('workplaces.category', $request->get('fieldName'))->get();
        }
        $arr = [];
        foreach ($workplaces as $workplace){
            $arr[$workplace->address][] = $workplace;
        }
        
        //        orderBy('name', 'desc')->
        return response($arr);
    }
}

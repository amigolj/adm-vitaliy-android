<?php

namespace App\Http\Controllers;

use App\Helpers\DocumentHelper;
use App\Http\Resources\ClientResource;
use App\Models\FamilyLaw;
use App\Models\Description;
use App\Models\Metro;
use App\Models\Workplace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\Place;

class AddressController extends Controller
{

    use DocumentHelper;

    protected $rules =
        [
            'name' => 'required|min:2|max:32',
//            'logo' => 'dimensions:min_width=100,min_height=100'
        ];
    protected $metro;


    public function __construct(Metro $metro)
    {
        $this->metro = $metro;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $addresses = Place::all();
        $metros = Metro::all();
//        dd($addresses->metros);
        return view('addresses',['addresses' => $addresses, 'metros'=>$metros]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->toArray();
        $validator = Validator::make($input, $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }

        try {
            $client = Place::create($input);
            $description = new Description($input);
            $client->description()->save($description);
        } catch (\Throwable $e) {
            return response()->json(
                ['errors' => $e->getMessage()]);
        }
        return response()->json(new ClientResource($client));
    }

    public function update(Request $request, $id)
    {
        $metros = explode(';', $request->get('metros'));
        $place = Place::where('place_id', $id)->first();
        $place->metros()->detach();
        foreach ($this->metro->getIdByName($metros) as $metro) {
            $place->metros()->attach($metro->metro_id);
        }
        return $place->load('metros');
    }

  
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Place::findOrFail($id);
        $post->delete();
        Workplace::where('place_id', $id)->delete();

        return response()->json($post);
    }

    public function sort(Request $request)
    {
        if ($request->get('sortType') == 1) {
            $clients = DB::table('clients')->get();
        } else {
            $clients = DB::table('clients')->orderBy($request->get('fieldName'), $request->get('sortType'))->get();
        }
        //        orderBy('name', 'desc')->
        return response($clients);
    }

    public function mapYandex()
    {
        $metros = Metro::all();
        return view('map',['metros'=>$metros]);
    }

    public function addAddressByMap(Request $request)
    {
        if ($request->exists('coords')) {
            $coords = explode(',', $request->get('coords'));
        }
        $place = Place::updateOrcreate(['address' => $request->get('address')],[
            'latitude' => $coords[0],
            'longitude' => $coords[1],
            'address' => $request->get('address')
        ]);
        $metros = explode(';', $request->get('metros'));
        foreach ($this->metro->getIdByName($metros) as $metro) {
            $place->metros()->attach($metro->metro_id);
        }
        
        return $place;
    }
}

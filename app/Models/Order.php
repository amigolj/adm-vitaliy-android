<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'price', 'surcharge', 'sender', 'recipient',
        'manager', 'point_issue', 'status', 'order_created_date',
        'order_issue_date', 'order_received_date', 'order_number', 'order_owner_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','order_owner_id');
    }
}

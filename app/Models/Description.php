<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Description extends Model
{
    public $timestamps = false;
    protected $table = 'descriptions';
    protected $primaryKey = 'description_id';

    protected $fillable = [
        'photo',
        'about',
        'user_id',
        'family',
        'category',
        'experience',
    ];

    public function getExperienceAttribute($value)
    {
        return  \Carbon\Carbon::parse($value)->diff(\Carbon\Carbon::now())->format('%y лет');
    }
}

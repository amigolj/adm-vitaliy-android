<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workshift extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\FamilyLaw', 'user_id', 'user_id');
    }
}

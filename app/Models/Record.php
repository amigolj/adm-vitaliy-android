<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{

    protected $primaryKey = 'record_id';
    public $timestamps = false;

    
    protected $dates = [
        'start',
        'record_date'
    ];

   
    protected $fillable = [
        'user_id',
        'workplace_id',
        'start',
        'status',
        'total',
    ];

    public function getRecordDateAttribute($value)
    {
        return  \Carbon\Carbon::parse($value)->format('d-m-Y');
    }

    public function getStartAttribute($value)
    {
        return  \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }
    
    public function setStartAttribute($value)
    {
        $this->attributes['start'] =  \Carbon\Carbon::parse($value)->timestamp;
    }
    
    public function user()
    {
        return $this->belongsTo('App\Models\FamilyLaw', 'user_id', 'user_id');
    }

    public function workshift()
    {
        return $this->belongsTo('App\Models\Workshift', 'workshift_id', 'workshift_id');
    }
    
    public function workplace()
    {
        return $this->belongsTo('App\Models\Workplace', 'workplace_id', 'workplace_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workplace extends Model
{
    protected $primaryKey = 'workplace_id';
    
    protected $fillable = ['workplace_id', 'place_id', 'category'];
    public $timestamps = false;

    public function categories()
    {
        return $this->belongsTo('App\Models\Category', 'category', 'id');
    }

    public function place()
    {
        return $this->belongsTo('App\Models\Place', 'place_id', 'place_id');
    }
  
}

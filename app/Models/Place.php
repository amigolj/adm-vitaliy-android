<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $primaryKey = 'place_id';
    public $timestamps = false;
    protected $fillable = [
        'place_id', 'latitude', 'longitude', 'address',
        'metro'
    ];

    public function metros()
    {
        return $this->belongsToMany('App\Models\Metro', 'places_metro', 'place_id','metro_id');
    }

    public function workplaces()
    {
        return $this->hasMany('App\Models\Workplace', 'place_id', 'place_id');
    }

    public function categories()
    {
        return $this->hasManyThrough(
            'App\Models\Category','App\Models\Workplace',
            'place_id', 'category', 'place_id'
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Metro extends Model
{
    protected $primaryKey = 'metro_id';
    public $timestamps = false;
    protected $fillable = ['metro_id', 'name'];

    public function getIdByName($metros)
    {
        return $this->whereIn('name', $metros)->get();
    }
}

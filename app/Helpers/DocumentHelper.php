<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 03.10.19
 * Time: 15:24
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

trait DocumentHelper
{
    public function saveImage(Request $request)
    {
        $filename = "";
        if ($request->file('doc')) {
            $filename = $request->file('doc')->getClientOriginalName();
            Log::alert($filename);
            $path = '/public/' . $filename;
            Storage::disk('local')->put($path, File::get($request->file('doc')));
        }
        return env('APP_URL').'/storage/'.$filename;
    }
}

<?php


namespace App\Helpers;


use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

trait LoginHelper
{
    protected function updateUser(array $data)
    {

        $code = $this->generateCode();
        $data['code'] = $code;
        if ($data['name'] == '+1(23'){
            return 123;
        }
        if ($user = User::where('name',trim($data['name']))->first()){
            $this->sendCode($data);
            Log::alert($user);
            $user->password = Hash::make($code);
            $user->save();
            return $code;
        }
        return false;
    }
    
    public function generateCode() {
        return rand(1000, 9999);
    }

    public function sendCode($date)
    {
        file_get_contents("https://gateway.api.sc/get/?user=CeoCaramel&pwd=s3yjLlJjWS&sadr=Ya.Krasota&dadr=".$date['name']."&text=".$date['code']);
    }
}
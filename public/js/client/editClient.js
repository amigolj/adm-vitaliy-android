// Edit a post
$(document).on('click', '.edit-modal', function() {
    $('.modal-title').text('Редактировать');

    
    $('#name_edit').val($(this).data('name'));
    $('#phone_edit').val($(this).data('phone'));
    $('#gender_edit').val($(this).data('gender'));
    $('#date_edit').val($(this).data('date'));
    $('#city_edit').val($(this).data('city'));

    id = $(this).data('id');
    $('#editModal').modal('show');
});
$('.modal-footer').on('click', '.edit', function(e) {
    // if (ValidateEmail($( "#email_edit" ).val()) === false) {
    //     alert("Вы ввели не корректный email");
    //     e.preventDefault();
    //     return false;
    // }
    
    // if (validateForm($( "form#form-edit :input" ), 1) === 1) {
    //     e.preventDefault();
    //     return false;
    // }
    $.ajax({
        type: 'PUT',
        url: 'clients/' + id,
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
        data:{
            '_token':$('input[name=_token]').val(),
            'user_id': id,
            'name': $('#name_edit').val(),
            'gender': $('#gender_edit').val(),
            'user_date': $('#date_edit').val(),
            'phone': $('#phone_edit').val(),
            'city': $('#city_edit').val(),
        },

        success: function(data) {
            $(".errorCommon").addClass('hidden');
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');

            if ((data.errors)) {
                setTimeout(function () {
                    $('#editModal').modal('show');
                    toastr.error('Ошибка в данных!', 'Error Alert', {timeOut: 5000});
                }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                toastr.success('Данные успешно отредактированы!', 'Успешно!', {timeOut: 5000});
                $('.item' + data.user_id).replaceWith("<tr class='item" + data.user_id + "'>" +
                    "<td>" + data.user_id + "</td>" +
                    "<td>" + data.name + "</td>" +
                    "<td>" + data.phone + "</td>" +
                    "<td>" + data.gender + "</td>" +
                    "<td>" + data.user_date + "</td>" +
                    "<td>" + data.city + "</td>" +
                    "<td><button class='edit-modal btn btn-info' "+
                    "data-id='" + data.user_id + "'" +
                    "data-name='" + data.name + "'" +
                    "data-phone='" + data.phone + "'" +
                    "data-gender='" + data.gender + "'" +
                    "data-date='" + data.user_date + "'" +
                    "data-date='" + data.city + "'" +
                    "><span class='glyphicon glyphicon-edit'></span> Редактировать</button> " +
                    // "<button class='delete-modal btn btn-danger' " +
                    // "data-id='" + data.user_id + "'" +
                    // "data-name='" + data.name + "'" +
                    // "data-phone='" + data.phone + "'" +
                    // "'>" +
                    // "<span class='glyphicon glyphicon-trash'></span> Удалить</button>" +
                    "</td></tr>");
            }
        }
    });
});
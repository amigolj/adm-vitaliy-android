
$(document).on('click', '.delete-modal', function() {
    $('.modal-title').text('Удалить');
    $('#id_delete').val($(this).data('id'));
    $('#deleteModal').modal('show');
    id = $('#id_delete').val();
});
$('.modal-footer').on('click', '.delete', function() {
    $.ajax({
        type: 'DELETE',
        url: 'families-law/' + id,
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function(data) {
            toastr.success('Документ видалено!', 'Успешно!', {timeOut: 5000});
            $('.item' + data['id']).remove();
        }
    });
});

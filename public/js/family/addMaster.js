$(document).on('click', '.add-modal', function() {
    $('.modal-title').text('Додати');
    $('#addModal').modal('show');
    // $('#category_add').change(function() {
    //     console.log($('#category_add option:selected').val());
    // })
});
$('.modal-footer').on('click', '.add', function(e) {
      

    var formData = new FormData();
    formData.append('_token', $('input[name=_token]').val());
    formData.append('doc', $('#doc_add').prop('files')[0]);
    formData.append('name', $('#name_add').val());
    formData.append('type', $('#type_add option:selected').val());
    formData.append('status', $('#status_add option:selected').val());
    formData.append('rate', $('#rate_add').val());
    formData.append('tags', $('#tag_add').val());
    $.ajax({ 
        type: 'POST',
        url: 'families-law',
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
        data: formData,
        contentType: false,
        cache: false,
        processData:false,
        success: function(data) {
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');
            $(".errorCommon").addClass('hidden');
         
            if ((data.errors)) {
                setTimeout(function () {
                    $('#addModal').modal('show');
                    toastr.error('Помилка в в данных!', data.errors, {timeOut: 5000});
                    $('#errorCommon').html('');
                    for (var key in data.errors) {
                        $('#errorCommon').append(data.errors[key]);
                    }
                }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                toastr.success('Документ успішно додан!', 'Успішно!', {timeOut: 5000});
                $('#postTableBody tr[class*="item"]:first').before("<tr class='item" + data.id + "'>" +
                    "<td>" + data.actual + "</td>" +
                    "<td>" + data.owner + "</td>" +
                    "<td>" + data.name + "</td>" +
                    "<td>" + data.status + "</td>" +
                    "<td>" + data.type + "</td>" +
                   
                    "<td>" + data.rate + "</td>" +
                    "<td>" + data.tags + "</td>" +
                    "<td>" +
                    " <button class='edit-modal btn btn-info' " +
                    "data-id='" + data.id + "' " +
                    "'>" +
                    "<span class='glyphicon glyphicon-edit'></span> Редагувати</button>" +
                    " <button class='delete-modal btn btn-success' " +
                    "data-id='" + data.id + "' " +
                    "'>" +
                    "<span  class='glyphicon glyphicon-download'></span>Скачати</button>" +
                    " <button class='delete-modal btn btn-danger' " +
                    "data-id='" + data.id + "' " +
                    "'>" +
                    "<span  class='glyphicon glyphicon-trash'></span> Видалити</button>"+
                    "</td></tr>");
            }
        },
    });
});

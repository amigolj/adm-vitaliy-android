$(document).on('change', '#category_group', function(e) {

    fieldName = $('#category_group option:selected').val()
    
    $("#postTableBody").addClass('hidden');
    $("#postTableBodySearchResult").empty();
            // $("#postTableBodySearchResult").html('<tr class="item"></tr>');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'GET',
        url: 'group/masters',
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}",
            "Content-Type": "text/plain; charset=utf-8"},
        data: {
            '_token':$('input[name=_token]').val(),
            'fieldName': fieldName,
        },

        success: function (data) {
            $("#postTableBody").addClass('hidden');
            $("#postTableBodySearchResult").removeClass('hidden');
            
            if ((data.errors)) {
                setTimeout(function () {
                    $('#editModal').modal('show');
                    toastr.error('Ошибка сортировки', 'Ошибка в данных!', {timeOut: 5000});
                    }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                // toastr.success('Заказ успешно найден!', 'Успешно!', {timeOut: 5000});
                // console.log("response"+data[0].id);
                $("#postTableBodySearchResult").empty();

                results = data;
                $.each(results, function (index, data) {
                    
                    handleNullUndifinedField(data);
                    button = "<button class='edit-modal btn btn-info' " +
                        "data-photo='" + data.photo + "' " +
                        "data-id='" + data.user_id + "' " +
                        "data-name='" + data.name + "' " +
                        "data-family='" + data.family + "' " +
                        "data-phone='" + data.phone + "' " +
                        "data-category='" + data.category + "' " +
                        "data-date='" + data.date + "' " +
                        "data-city='" + data.city + "' " +
                        "'>" +
                        "<span class='glyphicon glyphicon-edit'></span> Редактировать</button>";
                        // " <button class='delete-modal btn btn-danger' " +
                        // "data-id='" + data.id + "' " +
                        // "data-name='" + data.name + "" +
                        // "' >" +
                        // "<span  class='glyphicon glyphicon-trash'></span> Удалить</button>";

                        $('#postTableBodySearchResult').append("<tr class='item" + data.id + "'>" +
                                // $('#postTableBodySearchResult tr[class*="item"]:first').before("<tr class='item" + data.id + "'>" +
                                "<td>" + "<img width='100' height='100' src='https://adm.boobot.icu/storage/"+data.photo+"'/>" + "</td>" +
                                "<td>" + data.user_id + "</td>" +
                                "<td>" + data.name + "</td>" +
                                "<td>" + data.family + "</td>" +
                                "<td>" + data.phone + "</td>" +
                                "<td>" + data.category + "</td>" +
                                "<td>" + data.date + "</td>" +
                                "<td>" + data.city + "</td>" +
                                "<td>" + button + "</td></tr>");
                        });
                       
                    }
                }
            });
        
    });

    function handleNullUndifinedField(data){
        for (key in data) {
            data[key] = (data[key] == null) ? "" : data[key];
            data[key] = (data[key] === undefined) ? "" : data[key];
        }
    }
    

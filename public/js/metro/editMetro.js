// Edit a post
$(document).on('click', '.edit-modal', function() {
    $('.modal-title').text('Редактировать');
    $('#name_edit').val($(this).data('name'));
    id = $(this).data('metro_id');
    $('#editModal').modal('show');
});
$('.modal-footer').on('click', '.edit', function(e) {
    // if (ValidateEmail($( "#email_edit" ).val()) === false) {
    //     alert("Вы ввели не корректный email");
    //     e.preventDefault();
    //     return false;
    // }
    
    // if (validateForm($( "form#form-edit :input" ), 1) === 1) {
    //     e.preventDefault();
    //     return false;
    // }
    $.ajax({
        type: 'PUT',
        url: 'metros/' + id,
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
        data:{
            '_token':$('input[name=_token]').val(),
            'name': $('#name_edit').val(),
        },

        success: function(data) {
            $(".errorCommon").addClass('hidden');
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');

            if ((data.errors)) {
                setTimeout(function () {
                    $('#editModal').modal('show');
                    toastr.error('Ошибка в данных!', 'Error Alert', {timeOut: 5000});
                }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                toastr.success('Данные успешно отредактированы!', 'Успешно!', {timeOut: 5000});
                $('.item' + data.metro_id).replaceWith("<tr class='item" + data.metro_id + "'>" +
                    "<td>" + data.name + "</td>" +
                    "<td><button class='edit-modal btn btn-info' "+
                    "data-metro_id='" + data.metro_id + "'" +
                    "data-name='" + data.name + "'" +
                    "><span class='glyphicon glyphicon-edit'></span> Редактировать</button> " +
                    // "<button class='delete-modal btn btn-danger' " +
                    // "data-metro_id='" + data.metro_id + "'" +
                    // "data-name='" + data.name + "'" +
                    // "'>" +
                    // "<span class='glyphicon glyphicon-trash'></span> Удалить</button>" +
                    "</td></tr>");
            }
        }
    });
});
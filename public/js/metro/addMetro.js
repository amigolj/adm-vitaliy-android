$(document).on('click', '.add-modal', function() {
    $('.modal-title').text('Додати');
    $('#addModal').modal('show');
});
$('.modal-footer').on('click', '.add', function(e) {
      
    // if (validateForm($( "form#form-add :input" )) === 1) {
    //     e.preventDefault();
    //     return false;
    // }

    // if (ValidateEmail($( "#email_add" ).val()) === false) {
    //     alert("Вы ввели не корректный email");
    //     e.preventDefault(); 
    //     return false;
    // }
    var formData = new FormData();
    formData.append('_token', $('input[name=_token]').val());
    formData.append('name', $('#name_add').val());
    $.ajax({ 
        type: 'POST',
        url: 'metros',
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
        data: formData,
        contentType: false,
        cache: false,
        processData:false,
        success: function(data) {
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');
            $(".errorCommon").addClass('hidden');
            $('#name_add').val("");
            if ((data.errors)) {
                setTimeout(function () {
                    $('#addModal').modal('show');
                    toastr.error('Ошибка в в данных!', data.errors, {timeOut: 5000});
                    $('#errorCommon').html('');
                    for (var key in data.errors) {
                        $('#errorCommon').append(data.errors[key]);
                    }
                }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                toastr.success('Станция успешно добавлена!', 'Успешно!', {timeOut: 5000});
                $('#postTableBody tr[class*="item"]:first').before("<tr class='item" + data.metro_id + "'>" +
                    "<td>" + data.name + "</td>" +
                    "<td><button class='edit-modal btn btn-info' " +
                    "data-metro_id='" + data.metro_id + "' " +
                    "data-name='" + data.name + "' " +
                    "'>" +
                    "<span class='glyphicon glyphicon-edit'></span> Редактировать</button>" +
                    // " <button class='delete-modal btn btn-danger' " +
                    // "data-metro_id='" + data.metro_id + "' " +
                    // "data-name='" + data.name + "' >" +
                    // "<span  class='glyphicon glyphicon-trash'></span> Удалить</button>" +
                    "</td></tr>");
            }
        },
    });
});

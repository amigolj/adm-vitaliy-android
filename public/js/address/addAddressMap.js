// Edit a post
// $(document).on('click', '.edit-modal', function() {
//     $('.modal-title').text('Додати');
//     $("#listStation").empty();
//     ids = "";
//     $.each($(this).data('metro'), function (index, data) {
//         $("#listStation").append("<li class='stations' data-id='"+data.merto_id+"'>"+data.name+";<span class='deleteStation'><i class='fas fa-trash'></i></span></li>");
//         ids += ","+data.metro_id;
//     });
//     // 
//     // $.each($('.stations'), function(index, data){
//     //   
//     // });
//     id = $(this).data('id');
//     $('#editModal').modal('show');
// });
$('.modal-footer').on('click', '.edit', function(e) {
    $.ajax({
        type: 'POST',
        url: 'addresses/' + id,
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
        data:{
            '_token':$('input[name=_token]').val(),
            'metros':$('.stations').text()
        },

        success: function(data) {
            $(".errorCommon").addClass('hidden');
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');

            if ((data.errors)) {
                setTimeout(function () {
                    $('#editModal').modal('show');
                    toastr.error('Ошибка в данных!', 'Error Alert', {timeOut: 5000});
                }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                toastr.success('Данные успешно отредактированы!', 'Успешно!', {timeOut: 5000});
                metros = handleMetros(data.metros);
                // console.log(data.metros);
                $('.item' + data.place_id).replaceWith("<tr class='item" + data.place_id + "'>" +
                    "<td>" + data.address + "</td>" +
                    "<td class='list-metros-td'>" + metros + "</td>" +

                    "<td><button class='edit-modal btn btn-info' "+
                    "data-id='" + data.place_id + "' " +
                    "data-address='" + JSON.stringify(data.address) + "'" +
                    "data-metro='" + JSON.stringify(data.metros) + "'" +
                    "><span class='glyphicon glyphicon-edit'></span> Редактировать</button> " +
                    // "<button class='delete-modal btn btn-danger' " +
                    //
                    // "'>" +
                    // "<span class='glyphicon glyphicon-trash'></span> Удалить</button>" +
                    "</td></tr>");
            }
        }
    });
});

$(document).on('click', '#addMetroStation', function() {
    if($("#metroName").val()) {
        // alert($('#metroName [value="' + $("#metroName").val() + '"]').text());
        $("#listStation").append("<li class='stations'>" + $("#metroName").val() + ";<span class='deleteStation'><i class='fas fa-trash'></i></span></li>");
    }
});

$(document).on('click', '.deleteStation', function() {
    $(this).parent().remove();
});


$('#saveAddress').on('click', function(e) {
  
    var formData = new FormData();
    formData.append('_token', $('input[name=_token]').val());
    formData.append('metros',  $('.stations').text());
    formData.append('address', $('#address_add').val());
    formData.append('coords', $('#coords_add').val());
    $.ajax({ 
        type: 'POST',
        url: 'addaddressmap',
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
        data: formData,
        contentType: false,
        cache: false,
        processData:false,
        success: function(data) {
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');
            $(".errorCommon").addClass('hidden');
            $('#name_add').val("");
            if ((data.errors)) {
                setTimeout(function () {
                    $('#addModal').modal('show');
                    // toastr.error('Ошибка в в данных!', data.errors, {timeOut: 5000});
                    $('#errorCommon').html('');
                    for (var key in data.errors) {
                        $('#errorCommon').append(data.errors[key]);
                    }
                }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                $('#success').show().fadeOut(2500);
            }
        },
    });
});



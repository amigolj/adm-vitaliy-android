// Edit a post
$(document).on('click', '.edit-modal', function() {
    $('.modal-title').text('Редактировать');
    $("#listStation").empty();
    $('#address_edit').val($(this).data('address'));
    ids = "";
    $.each($(this).data('metro'), function (index, data) {
       $("#listStation").append("<li class='stations' data-id='"+data.merto_id+"'>"+data.name+";<span class='deleteStation'><i class='fas fa-trash'></i></span></li>");
        ids += ","+data.metro_id;
    });
    // 
    // $.each($('.stations'), function(index, data){
    //   
    // });
    id = $(this).data('id');
    $('#editModal').modal('show');
});
$('.modal-footer').on('click', '.edit', function(e) {
    $.ajax({
        type: 'PUT',
        url: 'addresses/' + id,
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
        data:{
            '_token':$('input[name=_token]').val(),
            'metros':$('.stations').text()
        },

        success: function(data) {
            $(".errorCommon").addClass('hidden');
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');

            if ((data.errors)) {
                setTimeout(function () {
                    $('#editModal').modal('show');
                    toastr.error('Ошибка в данных!', 'Error Alert', {timeOut: 5000});
                }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                toastr.success('Данные успешно отредактированы!', 'Успешно!', {timeOut: 5000});
                metros = handleMetros(data.metros);
                // console.log(data.metros);
                $('.item' + data.place_id).replaceWith("<tr class='item" + data.place_id + "'>" +
                    "<td>" + data.address + "</td>" +
                    "<td class='list-metros-td'>" + metros + "</td>" +
             
                    "<td><button class='edit-modal btn btn-info' "+
                    "data-id='" + data.place_id + "' " +
                    "data-address='" + JSON.stringify(data.address) + "'" +
                    "data-metro='" + JSON.stringify(data.metros) + "'" +
                    "><span class='glyphicon glyphicon-edit'></span> Редактировать</button> " +
                    // "<button class='delete-modal btn btn-danger' " +
                    //
                    // "'>" +
                    // "<span class='glyphicon glyphicon-trash'></span> Удалить</button>" +
                    "</td></tr>");
            }
        }
    });
});

$(document).on('click', '#addMetroStation', function() {
    if($("#metroName").val()) {
        // alert($('#metroName [value="' + $("#metroName").val() + '"]').text());
        $("#listStation").append("<li class='stations'>" + $("#metroName").val() + ";<span class='deleteStation'><i class='fas fa-trash'></i></span></li>");
    }
});

$(document).on('click', '.deleteStation', function() {
  $(this).parent().remove();  
})

function handleMetros(metros) {
    ul = '<ul class="list-metros-ul">';
    if(metros.length){
        $.each(metros, function (index, data) {
            ul += '<li>'+data.name+'</li>'   
        })
    }
    return ul += '</ul>';
}
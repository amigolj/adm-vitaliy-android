// Edit a post
$(document).on('click', '.edit-modal', function() {
    $('.modal-title').text('Редактировать');
    $('#record_id_edit').val($(this).data('record_id'));
    $('#date_edit').val($(this).data('date'));
    $('#start_edit').val($(this).data('start'));
    $('#user_edit').val($(this).data('user'));
    $('#master_edit').val($(this).data('master'));
    $('#status_edit').val($(this).data('status'));
    $('#total_edit').val($(this).data('total'));
    $('#workplace_id_edit').val($(this).data('workplace_id'));
    // $('#address_edit').val($(this).data('place_id'));

    id = $(this).data('record_id');
    $('#editModal').modal('show');
});
$('.modal-footer').on('click', '.edit', function(e) {
    $.ajax({
        type: 'PUT',
        url: 'requests/' + id,
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
        data:{
            '_token':$('input[name=_token]').val(),
            'start': $('#start_edit').val(),
            'user_id': $('#user_edit option:selected').val(),
            'master_id': $('#master_edit option:selected').val(),
            'status': $('#status_edit').val(),
            'total': $('#total_edit').val(),
            'workplace_id': $('#workplace_id_edit option:selected').val(),
            // 'address': $('#address_edit option:selected').val(),
        },

        success: function(data) {
            $(".errorCommon").addClass('hidden');
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');

            if ((data.errors)) {
                setTimeout(function () {
                    $('#editModal').modal('show');
                    toastr.error('Ошибка в данных!', 'Error Alert', {timeOut: 5000});
                }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                toastr.success('Данные успешно отредактированы!', 'Успешно!', {timeOut: 5000});
                $('.item' + data.record_id).replaceWith("<tr class='item" + data.record_id + "'>" +
                    "<td>" + data.record_id + "</td>" +
                    "<td>" + data.record_date + "</td>" +
                    "<td>" + data.category + "</td>" +
                    "<td>" + data.start + "</td>" +
                    "<td>" + data.user + "</td>" +
                    "<td>" + data.master + "</td>" +
                    "<td>" + data.status + "</td>" +
                    "<td>" + data.total + "</td>" +
                    "<td>" + data.workplace_id + "</td>" +
                    "<td>" + data.address + "</td>" +
                    "<td><button class='edit-modal btn btn-info' "+
                    "data-record_id='" + data.record_id + "'" +
                    "data-date='" + data.record_date + "'" +
                    "data-category='" + data.category + "'" +
                    "data-start='" + data.start + "'" +
                    "data-user='" + data.user + "'" +
                    "data-master='" + data.master + "'" +
                    "data-status='" + data.status + "'" +
                    "data-total='" + data.total + "'" +
                    "data-workplace_id='" + data.workplace_id + "'" +
                    "data-address='" + data.address + "'" +
                    "><span class='glyphicon glyphicon-edit'></span> Редактировать" +
                    "</button> " +
                    "</td></tr>");
            }
        }
    });
});
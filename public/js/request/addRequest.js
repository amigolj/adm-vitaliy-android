$(document).on('click', '.add-modal', function() {
    $('.modal-title').text('Додати');
    $('#addModal').modal('show');
    $('#category_add').change(function() {
        console.log($('#category_add option:selected').val());
    })
});
$('.modal-footer').on('click', '.add', function(e) {
      
    // if (validateForm($( "form#form-add :input" )) === 1) {
    //     e.preventDefault();
    //     return false;
    // }

    // if (ValidateEmail($( "#email_add" ).val()) === false) {
    //     alert("Вы ввели не корректный email");
    //     e.preventDefault(); 
    //     return false;
    // }
    var formData = new FormData();
    formData.append('_token', $('input[name=_token]').val());
    formData.append('photo', $('#photo_add').prop('files')[0]);
    formData.append('name', $('#name_add').val());
    formData.append('family', $('#family_add').val());
    formData.append('phone', $('#phone_add').val());
    formData.append('category', $('#category_add option:selected').val());
    formData.append('date', $( "#date_add" ).val());
    formData.append('city', $( "#city_add" ).val());
    $.ajax({ 
        type: 'POST',
        url: 'masters',
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
        data: formData,
        contentType: false,
        cache: false,
        processData:false,
        success: function(data) {
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');
            $(".errorCommon").addClass('hidden');
            $('#photo_add').val("");
            $('#name_add').val("");
            $('#phone_add').val("");
            $('#family_add').val("");
            $('#category_add').val("");
            $('#date_add').val("");
            $('#city_add').val("");
            if ((data.errors)) {
                setTimeout(function () {
                    $('#addModal').modal('show');
                    toastr.error('Ошибка в в данных!', data.errors, {timeOut: 5000});
                    $('#errorCommon').html('');
                    for (var key in data.errors) {
                        $('#errorCommon').append(data.errors[key]);
                    }
                }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                toastr.success('Мастер успешно добавлен!', 'Успешно!', {timeOut: 5000});
                $('#postTableBody tr[class*="item"]:first').before("<tr class='item" + data.user_id + "'>" +
                    "<td>" + "<img width='100' height='100' src='https://adm.boobot.icu/storage/"+data.photo+"'/>" + "</td>" +
                    "<td>" + data.user_id + "</td>" +
                    "<td>" + data.name + "</td>" +
                    "<td>" + data.family + "</td>" +
                    "<td>" + data.phone + "</td>" +
                    "<td>" + data.category + "</td>" +
                    "<td>" + data.date + "</td>" +
                    "<td>" + data.city + "</td>" +
                    "<td><button class='edit-modal btn btn-info' " +
                    "data-user_id='" + data.user_id + "' " +
                    "data-photo='" + data.photo + "' " +
                    "data-name='" + data.name + "' " +
                    "data-family='" + data.family + " ' " +
                    "data-phone='" + data.phone + " ' " +
                    "data-category='" + data.category + " ' " +
                    "data-date='" + data.date + " ' " +
                    "data-city='" + data.city + " ' " +
                    "'>" +
                    "<span class='glyphicon glyphicon-edit'></span> Редактировать</button>" +
                    // " <button class='delete-modal btn btn-danger' " +
                    // "data-user_id='" + data.user_id + "' " +
                    // "data-title='" + data.name + "' >" +
                    // "<span  class='glyphicon glyphicon-trash'></span> Удалить</button>" +
                    "</td></tr>");
            }
        },
    });
});


$(document).on('click', '.delete-modal', function() {
    $('.modal-title').text('Удалить');
    $('#id_delete').val($(this).data('id'));
    $('#price_delete').val($(this).data('price'));
    $('#deleteModal').modal('show');
    id = $('#id_delete').val();
});
$('.modal-footer').on('click', '.delete', function() {
    $.ajax({
        type: 'DELETE',
        url: 'users/' + id,
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function(data) {
            toastr.success('Пользователь успешно удален!', 'Успешно!', {timeOut: 5000});
            $('.item' + data['id']).remove();
        }
    });
});

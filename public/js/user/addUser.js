$(document).on('click', '.add-modal', function() {
    $('.modal-title').text('Додати');
    $('#addModal').modal('show');
});
$('.modal-footer').on('click', '.add', function(e) {
      
    if (validateForm($( "form#form-add :input" )) === 1) {
        e.preventDefault();
        return false;
    }

    if (ValidateEmail($( "#email_add" ).val()) === false) {
        alert("Вы ввели не корректный email");
        e.preventDefault(); 
        return false;
    }
    var formData = new FormData();
    formData.append('_token', $('input[name=_token]').val());
    formData.append('name', $('#name_add').val());
    formData.append('email', $('#email_add').val());
    formData.append('password', $('#password_add').val());
    formData.append('role', $( "#role_add option:selected" ).val());
    $.ajax({ 
        type: 'POST',
        url: 'users',
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
        data: formData,
        contentType: false,
        cache: false,
        processData:false,
        success: function(data) {
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');
            $(".errorCommon").addClass('hidden');
            $('#name_add').val("");
            $('#email_add').val("");
            $('#password_add').val("");
            if ((data.errors)) {
                setTimeout(function () {
                    $('#addModal').modal('show');
                    toastr.error('Ошибка в в данных!', data.errors, {timeOut: 5000});
                }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                toastr.success('Пользователь успешно добавлен!', 'Успешно!', {timeOut: 5000});
                $('#postTable tr[class*="item"]:first').before("<tr class='item" + data.id + "'>" +
                    "<td>" + data.id + "</td>" +
                    "<td>" + data.name + "</td>" +
                    "<td>" + data.email + "</td>" +
                    "<td>" + data.forUserRole + "</td>" +
                    "<td><button class='edit-modal btn btn-info' " +
                    "data-id='" + data.id + "' " +
                    "data-name='" + data.name + "' " +
                    "data-email='" + data.email + " ' " +
                    "data-role='" + data.role + " ' " +
                    "'>" +
                    "<span class='glyphicon glyphicon-edit'></span> Редактировать</button>" +
                    " <button class='delete-modal btn btn-danger' " +
                    "data-id='" + data.id + "' " +
                    "data-title='" + data.name + "' >" +
                    "<span  class='glyphicon glyphicon-trash'></span> Удалить</button></td></tr>");
            }
        },
    });
});

$(document).on('click', '.add-modal', function() {
    alert(window.currentUser.name);
    $('.modal-title').text('Додати');
    $('#addModal').modal('show'); 
    $("#status_add").hide();
    
    $("#checkbox_surcharge").click(function () {
        if($('#surcharge_add').hasClass('hidden')) {
            $('#surcharge_add').removeClass('hidden')
        }else{
            $('#surcharge_add').val('');

            $('#surcharge_add').addClass('hidden')
        }
    })
});
$('.modal-footer').on('click', '.add', function(e) {
    
    if (validateForm($( "form#form-add :input" )) === 1) {
        e.preventDefault();
        return false;
    }
    var formData = new FormData();
    formData.append('_token', $('input[name=_token]').val());
    formData.append('order_number', $('#order_number_add').val());
    formData.append('price', $('#price_add').val());
    formData.append('surcharge', $('#surcharge_add').val());
    formData.append('sender', $('#sender_add').val());
    formData.append('recipient', $('#recipient_add').val());
    // formData.append('manager', $('#manager_add').val());
    formData.append('point_issue', $('#point_issue_add option:selected').text());
    formData.append('order_created_date', $('#order_created_date_add').val()); 
    formData.append('status', $( "#status_add option:selected" ).text());
    // console.log($( "#status_add option:selected" ).text());
    $.ajax({
        type: 'POST',
        url: 'orders',
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
        data: formData,
        contentType: false,
        cache: false,
        processData:false,
        success: function(data) {
            $('#order_number_add').val('');
            $('#surcharge_add').val('');
            $('#sender_add').val('');
            $('#recipient_add').val('');
            $('#price_add').val('');
            $('#order_created_date_add').val('');
            $(".errorCommon").addClass('hidden');
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');
            inputs = $('.form-control');
            $(inputs).each(function( index ) {
                $(this).val('');
            })
            if ((data.errors)) {
                setTimeout(function () {
                    $('#addModal').modal('show');
                    toastr.error('Ошибка в данных!', 'Error Alert', {timeOut: 5000});
                }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                for (key in data) {
                    data[key] = (data[key] == null) ? "" : data[key];
                    data[key] = (data[key] === undefined) ? "" : data[key];
                }
                button = "<button class='edit-modal btn btn-info' " +
                "data-id='" + data.id + "' " +
                "data-order_number='" + data.order_number + "' " +
                "data-price='" + data.price + "' " +
                "data-surcharge='" + data.surcharge + "' " +
                "data-sender='" + data.sender + "' " +
                "data-recipient='" + data.recipient + "' " +
                "data-manager=''" +
                "data-point_issue='" + data.point_issue + "'" +
                "data-status='" + data.status + "'" +
                "data-order_created_date='" + data.order_created_date + "'" +
                "data-order_issue_date='" + data.order_issue_date + "'" +
                "'>"+
                "<span class='glyphicon glyphicon-edit'></span> Редактировать</button>";
                // " <button class='delete-modal btn btn-danger' " +
                // "data-id='" + data.id + "' " +
                // "data-order_number='" + data.order_number + "' " +
                // "data-title='" + data.manager + "' >" +
                // "<span  class='glyphicon glyphicon-trash'></span> Удалить</button>";

                
                if(data.role !== 'superadmin'){
                    button = '';
                }
                console.log("status"+data.status);
                toastr.success('Заказ успешно добавлен!', 'Успешно!', {timeOut: 5000});
                $('#postTableBody tr[class*="item"]:first').before("<tr class='item" + data.id + "'>" +
                    "<td id='"+data.id+"' class='td_id hidden'>" + data.id + "</td>" +
                    "<td>" + data.order_number + "</td>" +
                    "<td>" + data.price + "</td>" +
                    "<td>" + data.surcharge + "</td>" +
                    "<td>" + data.sender + "</td>" +
                    "<td>" + data.recipient + "</td>" +
                    "<td></td>" +
                    "<td>" + data.point_issue + "</td>" +
                    "<td>"+ data.status + "</td>" +
                    "<td>"+button+"</td></tr>");
            }
        },
    });
});
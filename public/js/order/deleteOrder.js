$(document).on('click', '.delete-modal', function() {
    $('.modal-title').text('Удалить');
    $('#id_delete').val($(this).data('id'));
    $('#order_number_delete').val($(this).data('order_number'));
    $('#price_delete').val($(this).data('price'));
    $('#deleteModal').modal('show');
    id = $('#id_delete').val();
});
$('.modal-footer').on('click', '.delete', function() {
    $.ajax({
        type: 'DELETE',
        url: 'orders/' + id,
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function(data) {
            toastr.success('Заказ успешно удален!', 'Успешно!', {timeOut: 5000});
            $('.item' + data['id']).remove();
        }
    });
});

$(document).on('click', '#search_reset', function() {
    
    
    $("#order-search").val('');
    $("#postTableBody").removeClass('hidden');
    $("#postTableBodySearchResult").addClass('hidden');
    // $("div[class^='item']").remove();
    $("#postTableBodySearchResult").html('<tr class="item"></tr>');
    // $("#postTableBodySearchResult").empty('');
});

$(document).on('input', '#order-search', function() {
    numberSymbolsToSearch = 3;

    needle = $("#order-search").val();
    if (needle.length == 0) {
        $("#postTableBody").removeClass('hidden');
        $("#postTableBodySearchResult").addClass('hidden');
        $("#postTableBodySearchResult").html('<tr class="item"></tr>')
    }
    if (needle.length >= numberSymbolsToSearch) {
        $("#postTableBody").addClass('hidden');
      
       
        $("#postTableBodySearchResult").empty();
        // $("#postTableBodySearchResult").html('<tr class="item"></tr>');
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: 'searchorders',
            headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}",
                "Content-Type": "text/plain; charset=utf-8"},
            data: {
                // '_token':$('input[name=_token]').val(),
                'needle': needle,
            },

            success: function (data) {
                $("#postTableBody").addClass('hidden');
                $("#postTableBodySearchResult").removeClass('hidden');


                if ((data.errors)) {
                    setTimeout(function () {
                        $('#editModal').modal('show');
                        toastr.error('Ошибка поиска', 'Ошибка в данных!', {timeOut: 5000});
                    }, 500);

                    if (data.errors.title) {
                        $('.errorTitle').removeClass('hidden');
                        $('.errorTitle').text(data.errors.title);
                    }
                    if (data.errors.content) {
                        $('.errorContent').removeClass('hidden');
                        $('.errorContent').text(data.errors.content);
                    }
                } else {
                    // toastr.success('Заказ успешно найден!', 'Успешно!', {timeOut: 5000});
                    // console.log("response"+data[0].id);
                    $("#postTableBodySearchResult").empty();

                    results = data;
                    $.each(results, function (index, data) {
                        
                        handleNullUndifinedField(data);
                        button = "<button class='edit-modal btn btn-info' " +
                            "data-id='" + data.id + "' " +
                            "data-order_number='" + data.order_number + "' " +
                            "data-price='" + data.price + "' " +
                            "data-surcharge='" + data.surcharge + "' " +
                            "data-sender='" + data.sender + "' " +
                            "data-recipient='" + data.recipient + "' " +
                            "data-manager='' " +
                            "data-point_issue='" + data.point_issue + "' " +
                            "data-status='" + data.status + "' " +
                            "data-order_created_date='" + data.order_created_date + "' " +
                            "data-order_issue_date='" + data.order_issue_date + "'" +
                            "'>" +
                            "<span class='glyphicon glyphicon-edit'></span> Редактировать</button>";
                            // " <button class='delete-modal btn btn-danger' " +
                            // "data-id='" + data.id + "' " +
                            // "data-order_number='" + data.order_number + "' " +
                            // "data-title='" + data.manager + "' >" +
                            // "<span  class='glyphicon glyphicon-trash'></span> Удалить</button>";

                        status = data.status;
                        if (data.role !== 'superadmin') {
                            button = '';
                        }
                        if (data.role !== 'superadmin' &&
                            data.role !== 'ego'
                        ) {
                            
                        
                            status =  "<select  class='all_status_list'"+
                            "data-selected='"+data.status+"'"+
                            "data-manager='"+$("#manager").val()+"'"+
                            "data-id='"+data.id+"'"+
                            "data-order_number='"+data.order_number+"'"+
                                ">"+
                                "<option value='Новый'>Новый</option>"+
                                "<option value='Принят'>Принят</option>"+        
                                "<option value='Выдан'>Выдан</option>"+        
                                "</select>"
                        }

                        $('#postTableBodySearchResult').append("<tr class='item" + data.id + "'>" +
                        // $('#postTableBodySearchResult tr[class*="item"]:first').before("<tr class='item" + data.id + "'>" +
                            "<td id='" + data.id + "' class='td_id hidden'>" + data.id + "</td>" +
                            "<td>" + data.order_number + "</td>" +
                            "<td>" + data.price + "</td>" +
                            "<td>" + data.surcharge + "</td>" +
                            "<td>" + data.sender + "</td>" +
                            "<td>" + data.recipient + "</td>" +
                            "<td>"+data.manager+"</td>" +
                            "<td>" + data.point_issue + "</td>" +
                            "<td class='td_list auto-tooltip'" +
                            "data-placement='top'" +
                            "data-toggle='tooltip'" +
                            "data-html='true'" +
                            "title="+data.status+">" 
                             + status + "</td>" +
                            "<td>" + button + "</td></tr>");
                    });
                    $(".all_status_list").each(function() {
                        $(this).val($(this).data('selected'));
                    });
                }
            }
        });
    }
});

function handleNullUndifinedField(data){
    for (key in data) {
        data[key] = (data[key] == null) ? "" : data[key];
        data[key] = (data[key] === undefined) ? "" : data[key];
    }
}
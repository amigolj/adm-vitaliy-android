$(document).on('click', '.edit-modal', function() {
    
    $('.modal-title').text('Редактировать');
    $('#order_number_edit').val($(this).data('order_number'));
    $('#price_edit').val($(this).data('price'));
    $('#surcharge_edit').val($(this).data('surcharge'));
    $('#sender_edit').val($(this).data('sender'));
    $('#recipient_edit').val($(this).data('recipient'));
    $('#manager_edit').val($(this).data('manager'));
    $('#point_issue_edit').val($(this).data('point_issue'));
    $('#status_edit').val($(this).data('status'));
    date_created = $(this).data('order_created_date');
    $('#order_created_date_edit').val(date_created.substr(0,10));
    id = $(this).data('id');
    $('#editModal').modal('show');
    $("#checkbox_surcharge_edit").click(function () {
        if($('#surcharge_edit').hasClass('hidden')) {
            $('#surcharge_edit').removeClass('hidden')
        }else{
            $('#surcharge_edit').val('');
            $('#surcharge_edit').addClass('hidden')
        }
    });
});
$('.modal-footer').on('click', '.edit', function(e) {
    if (validateForm($( "form#form-edit :input" )) === 1) {
        e.preventDefault();
        return false;
    }
  
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'PUT',
        url: 'orders/' + id,
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}",
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",},
        data:{
            '_token':$('input[name=_token]').val(),
            'id': $('#id').val(),
            'order_number': $('#order_number_edit').val(),
            'price': $('#price_edit').val(),
            'surcharge': $('#surcharge_edit').val(),
            'sender': $('#sender_edit').val(),
            'recipient': $('#recipient_edit').val(),
            'manager': $('#manager_edit').val(),
            'point_issue': $('#point_issue_edit option:selected').text(),
            'order_created_date': $('#order_created_date_edit').val(),
            'status': $( "#status_edit option:selected" ).text(),
        },
       
        success: function(data) {
            $(".errorCommon").addClass('hidden');
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');

            if ((data.errors)) {
                setTimeout(function () {
                    $('#editModal').modal('show');
                    toastr.error('Ошибка', 'Ошибка в данных!', {timeOut: 5000});
                }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                for (key in data) {
                    data[key] = (data[key] == null) ? "" : data[key];
                    data[key] = (data[key] === undefined) ? "" : data[key];
                }
                console.log("status"+data.status);
                toastr.success('Заказ успешно обновлен!', 'Успешно!', {timeOut: 5000});
                $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'>" +
                    "<td id='"+data.id+"' class='td_id hidden'>" + data.id + "</td>" +
                    "<td>" + data.order_number + "</td>" +
                    "<td>" + data.price + "</td>" +
                    "<td>" + data.surcharge + "</td>" +
                    "<td>" + data.sender + "</td>" +
                    "<td>" + data.recipient + "</td>" +
                    "<td>" + data.manager + "</td>" +
                    "<td>" + data.point_issue + "</td>" +
                    "<td>" + data.status + "</td>" +
                    "<td><button class='edit-modal btn btn-info' "+
                    "data-id='" + data.id + "'" +
                    "data-order_number='" + data.order_number + "'" +
                    "data-price='" + data.price + "'" +
                    "data-surcharge='" + data.surcharge + "'" +
                    "data-sender='" + data.sender + "'" +
                    "data-recipient='" + data.recipient + "'" +
                    "data-manager='" + data.manager + "'" +
                    "data-point_issue='" + data.point_issue + "'" +
                    "data-status='" + data.status + "'" +
                    "data-order_created_date='" + data.order_created_date + "'" +
                    "data-order_issue_date='" + data.order_issue_date +
                    " '> " +
                    "<span class='glyphicon glyphicon-edit'></span> Редактировать</button> " +
                    // "<button class='delete-modal btn btn-danger' " +
                    // "data-id='" + data.id + "'" +
                    // "data-order_number='" + data.order_number + "'" +
                    // "data-price='" + data.price + "'" +
                    // "data-surcharge='" + data.surcharge + "'" +
                    // "data-sender='" + data.sender + "'" +
                    // "data-recipient='" + data.recipient + "'" +
                    // "data-manager='" + data.manager + "'" +
                    // "data-point_issue='" + data.point_issue + "'" +
                    // "data-status='" + data.status +
                    // "'>" +
                    // "<span class='glyphicon glyphicon-trash'></span> Удалить</button>" +
                    "</td></tr>");
            }
        }
    });
});

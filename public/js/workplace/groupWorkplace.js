$(document).on('change', '#workplace_group', function(e) {

    fieldName = $('#workplace_group option:selected').val();
    $(".allWorkPlacesTables").addClass('hidden');
    $(".table").addClass('hidden');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'GET',
        url: 'group/workplaces',
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}",
            "Content-Type": "text/plain; charset=utf-8"},
        data: {
            '_token':$('input[name=_token]').val(),
            'fieldName': fieldName,
        },

        success: function (data) {
            $("#postTableBody").addClass('hidden');
                      
            if ((data.errors)) {
                setTimeout(function () {
                    $('#editModal').modal('show');
                    toastr.error('Ошибка сортировки', 'Ошибка в данных!', {timeOut: 5000});
                    }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                // toastr.success('Заказ успешно найден!', 'Успешно!', {timeOut: 5000});
                // console.log("response"+data[0].id);
                $("#postTableBodySearchResult").empty();

                results = data;
                $.each(results, function (index, data) {
                    
                    handleNullUndifinedField(data);
                        thead = formationTheadTable(data, index);
                        // $('.table-responsive').append("<table id='sd'>");
                        // console.log(data);
                        body ="";
                        $.each(data, function (index, data2) {
                            buttons = formationButtons(data2);

                        body += "<tr class='item" + data2.place_id + "'>" +
                                "<td>" + data2.workplace_id + "</td>" +
                                "<td>" + data2.category + "</td>" +
                                "<td>" + buttons + "</td>" +
                            "</tr>";
                        });
                        $('.table-responsive').append(thead+body+"</tbody></table>");
                        });
                    }
                }
            });
        
    });

    function handleNullUndifinedField(data){
        for (key in data) {
            data[key] = (data[key] == null) ? "" : data[key];
            data[key] = (data[key] === undefined) ? "" : data[key];
        }
    }
    
    function formationButtons(data2) {
        return button =  "<button class='edit-modal btn btn-info' "+
            "data-category_id='" + data2.workplace_id + "'" +
            "data-place_id='" + data2.place_id + "'" +
            "data-category='" + data2.category + "'" +
            "><span class='glyphicon glyphicon-edit'></span> Редактировать</button> " ;
            // "<button class='delete-modal btn btn-danger' " +
            // "data-place_id='" + data2.place_id + "'" +
            // "data-workplace_id='" + data2.workplace_id + "'" +
            // "data-category='" + data2.category + "'" +
            // "'>" +
            // "<span class='glyphicon glyphicon-trash'></span> Удалить</button>";
    }
    
    function formationTheadTable(data, index) {
        return thead = "<table class='table' id='postTable' >"+
        "<thead>"+
        "<tr class='head_address'><td  colspan='4'><h3>"+index+"</h3></td></tr>"+
        "<th><span class='col_name'>workplace_id</span><i class='fas fa-sort-up hidden'></i><i class='fas fa-sort-down hidden'></i></th>"+
        "<th><span class='col_name'>category</span><i class='fas fa-sort-up hidden'></i><i class='fas fa-sort-down hidden'></i></th>"+
        "</thead>" +
        "<tbody id='postTableBody"+data.place_id+"' class='allWorkPlacesTables'>";
    }
    

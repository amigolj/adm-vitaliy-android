// Edit a post
$(document).on('click', '.edit-modal', function() {
    $('.modal-title').text('Редактировать');
    $('#place_id_edit').val($(this).data('place_id'));
    $('#category_edit').val( $(this).data('category'));
    id = $(this).data('workplace_id');
    $('#editModal').modal('show');
});
$('.modal-footer').on('click', '.edit', function(e) {
    // if (ValidateEmail($( "#email_edit" ).val()) === false) {
    //     alert("Вы ввели не корректный email");
    //     e.preventDefault();
    //     return false;
    // }
    
    // if (validateForm($( "form#form-edit :input" ), 1) === 1) {
    //     e.preventDefault();
    //     return false;
    // }
    $.ajax({
        type: 'PUT',
        url: 'workplaces/' + id,
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
        data:{
            '_token':$('input[name=_token]').val(),
            'place_id': $('#place_id_edit').val(),
            'category': $('#category_edit').val(),
        },

        success: function(data) {
            $(".errorCommon").addClass('hidden');
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');

            if ((data.errors)) {
                setTimeout(function () {
                    $('#editModal').modal('show');
                    toastr.error('Ошибка в данных!', 'Error Alert', {timeOut: 5000});
                }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                toastr.success('Данные успешно отредактированы!', 'Успешно!', {timeOut: 5000});
                $('.item' + data.place_id+data.workplace_id).replaceWith("<tr class='item" + data.place_id + data.workplace_id + "'>" +
                    "<td>" + data.workplace_id + "</td>" +
                    "<td>" + data.category + "</td>" +
                    "<td>" +
                    "<button class='edit-modal btn btn-info' "+
                    "data-workplace_id='" + data.workplace_id + "'" +
                    "data-place_id='" + data.place_id + "'" +
                    "data-category='" + data.category + "'" +
                    "><span class='glyphicon glyphicon-edit'></span> Редактировать</button> " +
                    // "<button class='delete-modal btn btn-danger' " +
                    // "data-place_id='" + data.place_id + "'" +
                    // "data-workplace_id='" + data.workplace_id + "'" +
                    // "data-category='" + data.category + "'" +
                    // "'>" +
                    // "<span class='glyphicon glyphicon-trash'></span> Удалить</button>" +
                    "</td></tr>");
            }
        }
    });
});
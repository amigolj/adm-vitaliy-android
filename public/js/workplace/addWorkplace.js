$(document).on('click', '.add-modal', function() {
    $('.modal-title').text('Додати');
    $('#addModal').modal('show');
});
$('.modal-footer').on('click', '.add', function(e) {
      
    // if (validateForm($( "form#form-add :input" )) === 1) {
    //     e.preventDefault();
    //     return false;
    // }

    // if (ValidateEmail($( "#email_add" ).val()) === false) {
    //     alert("Вы ввели не корректный email");
    //     e.preventDefault(); 
    //     return false;
    // }
    var formData = new FormData();
    formData.append('_token', $('input[name=_token]').val());
    formData.append('place_id', $('#place_add option:selected').val());
    formData.append('category', $('#category_add option:selected').val());

    $.ajax({ 
        type: 'POST',
        url: 'workplaces',
        headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
        data: formData,
        contentType: false,
        cache: false,
        processData:false,
        success: function(data) {
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');
            $(".errorCommon").addClass('hidden');
            $('#name_add').val("");
            if ((data.errors)) {
                setTimeout(function () {
                    $('#addModal').modal('show');
                    toastr.error('Ошибка в в данных!', data.errors, {timeOut: 5000});
                    $('#errorCommon').html('');
                    for (var key in data.errors) {
                        $('#errorCommon').append(data.errors[key]);
                    }
                }, 500);

                if (data.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(data.errors.title);
                }
                if (data.errors.content) {
                    $('.errorContent').removeClass('hidden');
                    $('.errorContent').text(data.errors.content);
                }
            } else {
                toastr.success('Рабочее место успешно добавлена!', 'Успешно!', {timeOut: 5000});
                if($('#postTableBody'+data.place_id).length) {
                    $('#postTableBody' + data.place_id + ' tr[class*="item' + data.place_id + '"]:first').before("<tr class='item" + data.place_id + data.workplace_id + "'>" +
                "<td>" + data.workplace_id + "</td>" +
                "<td>" + data.category + "</td>" +
                "<td><button class='edit-modal btn btn-info' " +
                "data-place_id='" + data.place_id + "' " +
                "data-category='" + data.category + "' " +
                "data-workplace_id='" + data.workplace_id + "' " +
                "'>" +
                "<span class='glyphicon glyphicon-edit'></span> Редактировать</button>" +
                // " <button class='delete-modal btn btn-danger' " +
                // "data-category='" + data.category + "' " +
                // "data-workplace_id='" + data.workplace_id + "' " +
                // "'>" +
                // "<span  class='glyphicon glyphicon-trash'></span> Удалить</button>" +
                        "</td></tr>");
            }  else {

                    thead = formationTheadTableAdd(data);
                    buttons = formationButtons(data);
                    body = "<tr class='item" + data.place_id+data.workplace_id + "'>" +
                        "<td>" + data.workplace_id + "</td>" +
                        "<td>" + data.category + "</td>" +
                        "<td>" + buttons + "</td>" +
                        "</tr>";
                    $('.table-responsive').append(thead+body+"</tbody></table>");


                    // $('#postTableBody' + data.place_id + ' tr[class*="item' + data.place_id + '"]:first').before("<tr class='item" + data.place_id + data.workplace_id + "'>" +
                    //     "<td>" + data.workplace_id + "</td>" +
                    //     "<td>" + data.category + "</td>" +
                    //     "<td><button class='edit-modal btn btn-info' " +
                    //     "data-place_id='" + data.place_id + "' " +
                    //     "data-category='" + data.category + "' " +
                    //     "data-workplace_id='" + data.workplace_id + "' " +
                    //     "'>" +
                    //     "<span class='glyphicon glyphicon-edit'></span> Редактировать</button>" +
                    //     " <button class='delete-modal btn btn-danger' " +
                    //     "data-category='" + data.category + "' " +
                    //     "data-workplace_id='" + data.workplace_id + "' " +
                    //     "'>" +
                    //     "<span  class='glyphicon glyphicon-trash'></span> Удалить</button></td></tr>");
                }    
    }
        },
    });
});

function formationTheadTableAdd(data) {
    return thead = "<table class='table' id='postTable' >"+
        "<thead>"+
        "<tr class='head_address'><td  colspan='4'><h3>"+data.place.address+"</h3></td></tr>"+
        "<th><span class='col_name'>workplace_id</span><i class='fas fa-sort-up hidden'></i><i class='fas fa-sort-down hidden'></i></th>"+
        "<th><span class='col_name'>category</span><i class='fas fa-sort-up hidden'></i><i class='fas fa-sort-down hidden'></i></th>"+
        "</thead>" +
        "<tbody id='postTableBody"+data.place_id+"' class='allWorkPlacesTables'>";
}
    



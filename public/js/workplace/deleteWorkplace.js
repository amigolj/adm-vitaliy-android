
$(document).on('click', '.delete-modal', function() {
    $('.modal-title').text('Удалить');
    $('#id_delete').val($(this).data('workplace_id'));
    $('#deleteModal').modal('show');
    id = $('#id_delete').val();
});
$('.modal-footer').on('click', '.delete', function() {
    $.ajax({
        type: 'DELETE',
        url: 'workplaces/' + id,
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function(data) {
            toastr.success('Рабочее место успешно удалено!', 'Успешно!', {timeOut: 5000});
            $('.item' + data['place_id'] + data['workplace_id']).remove();
        }
    });
});
